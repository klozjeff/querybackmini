<?php

/**
 * Posts controller
 *
 * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
 * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
 */

class LoggersController extends Controller{


    public function beforeAction(){

        parent::beforeAction();

        Config::addJsConfig('curPage', "loggers");

        $action  = $this->request->param('action');
        $actions = ['create', 'update','import'];
        $this->Security->requirePost($actions);

        switch($action){
            case "create":
                $this->Security->config("form", [ 'fields' => ['feed_name','logger_group','content','profile_picture']]);
                break;
            case "update":
                $this->Security->config("form", [ 'fields' => ['post_id', 'title', 'content']]);
                break;
			case "import":
                $this->Security->config("form", [ 'fields' => ['file']]);
                break;
            case "delete":
                $this->Security->config("validateCsrfToken", true);
                $this->Security->config("form", [ 'fields' => ['post_id']]);
                break;
        }
    }

    /**
     * Show Feeds page
     *
     */
    public function index(){

        $pageNum  = $this->request->query("page");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'loggers/index.php', ['pageNum' => $pageNum]);
    }
/**

* Import Loggers
* File

**/
	 public function import(){

        $fileData  = $this->request->data("file");

        $file = $this->loggers->import(Session::getUserId(), $fileData);

        if(!$file){
            $this->view->renderErrors($this->loggers->errors());
        }
		else{
            Session::set('import-success', "Contacts Imported Successfully");
        }

        return $this->redirector->root("Loggers");
    }
  

    public function isAuthorized(){

        $action = $this->request->param('action');
        $role = Session::getUserRole();
        $resource = "loggers";

        // only for admins
        Permission::allow('admin', $resource, ['*']);

        // only for normal users
        Permission::allow('user', $resource, ['index', 'view', 'newPost', 'create']);
        Permission::allow('user', $resource, ['update', 'delete'], 'owner');

        $postId  = ($action === "delete")? $this->request->param("args")[0]: $this->request->data("post_id");
        if(!empty($postId)){
            $postId = Encryption::decryptId($postId);
        } 

        $config = [
            "user_id" => Session::getUserId(),
            "table" => "loggers",
            "id" => $postId
        ];

        return Permission::check($role, $resource, $action, $config);
    }
}

<?php

/**
 * Posts controller
 *
 * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
 * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
 */

class PayshopController extends Controller{


    public function beforeAction(){

        parent::beforeAction();

        Config::addJsConfig('curPage', "payshop");

        $action  = $this->request->param('action');
        $actions = ['create', 'update'];
        $this->Security->requirePost($actions);

        switch($action){
           
        }
    }

    /**
     * Show Payshop/Payments page
     *
     */
    public function index(){

        $pageNum  = $this->request->query("page");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'payshop/index.php', ['pageNum' => $pageNum]);
    }

  

    public function isAuthorized(){

        $action = $this->request->param('action');
        $role = Session::getUserRole();
        $resource = "loggers";

        // only for admins
        Permission::allow('admin', $resource, ['*']);

        // only for normal users
        Permission::allow('user', $resource, ['index', 'view', 'newPost', 'create']);
        Permission::allow('user', $resource, ['update', 'delete'], 'owner');

        $postId  = ($action === "delete")? $this->request->param("args")[0]: $this->request->data("post_id");
        if(!empty($postId)){
            $postId = Encryption::decryptId($postId);
        } 

        $config = [
            "user_id" => Session::getUserId(),
            "table" => "qb_payments",
            "id" => $postId
        ];

        return Permission::check($role, $resource, $action, $config);
    }
}

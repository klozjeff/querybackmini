<?php

/**
 * Posts controller
 *
 * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
 * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
 */

class QueriesController extends Controller{


    public function beforeAction(){

        parent::beforeAction();

        Config::addJsConfig('curPage', "queries");

        $action  = $this->request->param('action');
        $actions = ['create', 'update','email','forwardQuery'];
        $this->Security->requirePost($actions);
	    $this->Security->requireAjax('archive','send','uploads');

        switch($action){
            case "create":
                $this->Security->config("form", [ 'fields' => ['Query_name','logger_group','content','profile_picture']]);
                break;
		  case "email":
                $this->Security->config("form", [ 'fields' => ['email_id','email_subject','email_body','Query_id']]);
                break;		
		case "forwardQuery":
                $this->Security->config("form", [ 'fields' => ['logger_group','Query_id']]);
                break;
        case "update":
                $this->Security->config("form", [ 'fields' => ['post_id', 'title', 'content']]);
                break;
		case "archive":
                $this->Security->config("form", [ 'fields' => ['Query_id']]);
                break;
		case "send":
                $this->Security->config("form",['fields' => ['sender_id','chat_id','message','filename','texttype']]);
                break;
		case "uploads":
                $this->Security->config("form", [ 'fields' => ['file']]);
                break;
            case "delete":
                $this->Security->config("validateCsrfToken", true);
                $this->Security->config("form", [ 'fields' => ['post_id']]);
                break;
        }
    }

    /**
     * Show Queries page
     *
     */
    public function index(){

     
        $pageNum  = $this->request->query("page");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'Queries/index.php', ['pageNum' => $pageNum]);
    }

    /**
     * view a Query
     *
     * @param integer|string $chatId
     */
    public function view($senderId=0,$chatId = 0){

    
        if(!$this->Queries->existsUnique('qb_chats','unique_chat_id',$chatId)){
            return $this->error(404);
        }

        Config::addJsConfig('curPage', ["queries"]);
        Config::addJsConfig('chatId',$chatId);
        $action  = $this->request->query('action');
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'Queries/viewChat.php', ["action"=> $action,"senderId" => $senderId,"chatId" => $chatId]);
    }

 

    /**
     * creates a new Query
     *
     */
    public function create(){

        $title    = $this->request->data("Query_name");
		$loggers  = $this->request->data("logger_group");
        $content  = $this->request->data("content");
		$fileData = $this->request->data("profile_picture");

        $result=$this->Queries->create(Session::getUserUniqueID(),Session::getUserCompany(),$title,$loggers,$content,$fileData);

        if(!$result){
            Session::set('Queries-errors', $this->Queries->errors());
        }
		else{
            Session::set('Queries-success', "Query has been created");
        }

        return $this->redirector->root("Queries/newQuery");
    }
	
	
	  /**
     * Email Query
     *
     */
    public function email(){

        $email_id    = $this->request->data("email_id");
		$email_subject  = $this->request->data("email_subject");
        $email_body  = $this->request->data("email_body");
		$chatId = $this->request->data("Query_id");

        $result=$this->Queries->email(Session::getUserUniqueID(),Session::getUserCompany(),$email_id,$email_subject,$email_body,$chatId);

        if(!$result){
            Session::set('email-errors', $this->Queries->errors());
        }
		else{
            Session::set('email-success', "Email has been created");
        }

        return $this->redirector->root("Queries");
    }
	
	  /**
     * Email Query
     *
     */
    public function forwardQuery(){

		$logger_group  = $this->request->data("logger_group");
		$chatId = $this->request->data("Query_id");

        $result=$this->Queries->forwardQuery(Session::getUserUniqueID(),Session::getUserCompany(),$logger_group,$chatId);

        if(!$result){
            Session::set('forward-errors', $this->Queries->errors());
        }
		else{
            Session::set('forward-success', "Query Successfully Forwarded");
        }

        return $this->redirector->root("Queries");
    }
	

    /**
     * update a post
     *
     */
    public function update(){

        $postId  = $this->request->data("post_id");
        $title   = $this->request->data("title");
        $content = $this->request->data("content");

        $postId = Encryption::decryptId($postId);

        if(!$this->post->exists($postId)){
            return $this->error(404);
        }

        $post = $this->post->update($postId, $title, $content);

        if(!$post){

            Session::set('posts-errors', $this->post->errors());
            return $this->redirector->root("Posts/View/" . urlencode(Encryption::encryptId($postId)) . "?action=update");

        }else{
            return $this->redirector->root("Posts/View/" . urlencode(Encryption::encryptId($postId)));
        }
    }
	
public function archive(){
       $chatId = $this->request->data("Query_id");
       $QueriesArchive = $this->Queries->archiveById($chatId);

        if(!$QueriesArchive){
            $this->view->renderErrors($this->Queries->errors());
        } else{
            $QueriesAll= $this->Queries->getAll();
            $QueriesHTML = $this->view->render(Config::get('VIEWS_PATH') . 'Queries/Queries.php', array("Queries" => $QueriesAll['Queries']));
            $this->view->renderJson(array("data" => ["QueriesHTML" => $QueriesHTML]));
        }
    }
 public function uploads(){

        $fileData=$this->request->data("file");
        $file = $this->queries->uploads(Session::getUserId(),$fileData);
        if(!$file){
            $this->view->renderErrors($this->queries->errors());
        }
else
{
		$this->view->renderJson(array("file" =>$fileData));
}
      
    }
public function send(){
       $senderId = $this->request->data("sender_id");
	   $chatId = $this->request->data("chat_id");
	   $message = $this->request->data("message");
	   $filename = $this->request->data("filename");
	   $texttype = $this->request->data("texttype");
       $sendMessage = $this->Queries->sendMessage($senderId,$chatId,$message,$filename,$texttype);

        if(!$sendMessage){
            $this->view->renderErrors($this->Queries->errors());
        } else{
            $messages = $this->controller->queries->getChatMessages($senderId,$chatId);
            $messagesHTML = $this->view->render(Config::get('VIEWS_PATH') . 'queries/chat.php', array("messages" =>$messages));
            $this->view->renderJson(array("data" => ["messagesHTML" => $messagesHTML]));
        }
    }
  

    public function isAuthorized(){

        $action = $this->request->param('action');
        $role = Session::getUserRole();
        $resource = "posts";

        // only for admins
        Permission::allow('admin', $resource, ['*']);

        // only for normal users
        Permission::allow('user', $resource, ['index', 'view', 'newPost', 'create']);
        Permission::allow('user', $resource, ['update', 'delete'], 'owner');

        $postId  = ($action === "delete")? $this->request->param("args")[0]: $this->request->data("post_id");
        if(!empty($postId)){
            $postId = Encryption::decryptId($postId);
        } 

        $config = [
            "user_id" => Session::getUserId(),
            "table" => "posts",
            "id" => $postId
        ];

        return Permission::check($role, $resource, $action, $config);
    }
}

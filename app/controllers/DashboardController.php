<?php

/**
 * User controller
 *
  * User Class
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
 */

class DashboardController extends Controller{

    public function beforeAction(){

        parent::beforeAction();

        $action = $this->request->param('action');
        $actions = [];
        $this->Security->requirePost($actions);

      
    }

    /**
     * show dashboard page
     *
     */
    public function index(){

        Config::addJsConfig('curPage', "dashboard");
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'dashboard/index.php');
    }

    
    public function isAuthorized(){
        return true;
    }
}

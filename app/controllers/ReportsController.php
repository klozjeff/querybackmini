<?php

/**
 * Reports controller
 *
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
 */

class ReportsController extends Controller{


    public function beforeAction(){

        parent::beforeAction();

        //Config::addJsConfig('curPage', "reports");
    }

    /**
     * Show Reports page
     *
     */
    public function index(){

        $pageNum  = $this->request->query("page");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'Reports/index.php', ['pageNum' => $pageNum]);
    }

  

    public function isAuthorized(){

        $action = $this->request->param('action');
        $role = Session::getUserRole();
        $resource = "reports";

        // only for admins
        Permission::allow('admin', $resource, ['*']);

        // only for normal users
        Permission::allow('user', $resource, ['index', 'view', 'newPost', 'create']);
        Permission::allow('user', $resource, ['update', 'delete'], 'owner');

        $postId  = ($action === "delete")? $this->request->param("args")[0]: $this->request->data("post_id");
        if(!empty($postId)){
            $postId = Encryption::decryptId($postId);
        } 

        $config = [
            "user_id" => Session::getUserId(),
            "table" => "reports",
            "id" => $postId
        ];

        return Permission::check($role, $resource, $action, $config);
    }
}

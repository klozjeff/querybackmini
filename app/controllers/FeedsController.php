<?php

/**
 * Posts controller
 *
 * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
 * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
 */

class FeedsController extends Controller{


    public function beforeAction(){

        parent::beforeAction();

        Config::addJsConfig('curPage', "feeds");

        $action  = $this->request->param('action');
        $actions = ['create', 'update','email','forwardFeed'];
        $this->Security->requirePost($actions);
	    $this->Security->requireAjax('archive');

        switch($action){
            case "create":
                $this->Security->config("form", [ 'fields' => ['feed_name','logger_group','content','profile_picture']]);
                break;
		  case "email":
                $this->Security->config("form", [ 'fields' => ['email_id','email_subject','email_body','feed_id']]);
                break;		
		case "forwardFeed":
                $this->Security->config("form", [ 'fields' => ['logger_group','feed_id']]);
                break;
        case "update":
                $this->Security->config("form", [ 'fields' => ['post_id', 'title', 'content']]);
                break;
		case "archive":
                $this->Security->config("form", [ 'fields' => ['feed_id']]);
                break;
            case "delete":
                $this->Security->config("validateCsrfToken", true);
                $this->Security->config("form", [ 'fields' => ['post_id']]);
                break;
        }
    }

    /**
     * Show Feeds page
     *
     */
    public function index(){

        // clear all notifications
        $this->user->clearNotifications(Session::getUserId(), $this->post->table);

        $pageNum  = $this->request->query("page");

        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'feeds/index.php', ['pageNum' => $pageNum]);
    }

    /**
     * view a Feed
     *
     * @param integer|string $feedId
     */
    public function view($feedId = 0){

    
        if(!$this->feeds->existsUnique('qb_company_feeds','unique_id_feeds',$feedId)){
            return $this->error(404);
        }

        Config::addJsConfig('curPage', ["feeds"]);
        Config::addJsConfig('feedId',$feedId);
        $action  = $this->request->query('action');
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'feeds/viewFeed.php', ["action"=> $action, "feedId" => $feedId]);
    }

    /**
     * show new post form
     */
    public function newFeed(){
        $this->view->renderWithLayouts(Config::get('VIEWS_PATH') . "layout/default/", Config::get('VIEWS_PATH') . 'feeds/newFeed.php');
    }

    /**
     * creates a new Feed
     *
     */
    public function create(){

        $title    = $this->request->data("feed_name");
		$loggers  = $this->request->data("logger_group");
        $content  = $this->request->data("content");
		$fileData = $this->request->data("profile_picture");

        $result=$this->feeds->create(Session::getUserUniqueID(),Session::getUserCompany(),$title,$loggers,$content,$fileData);

        if(!$result){
            Session::set('feeds-errors', $this->feeds->errors());
        }
		else{
            Session::set('feeds-success', "Feed has been created");
        }

        return $this->redirector->root("Feeds/newFeed");
    }
	
	
	  /**
     * Email Feed
     *
     */
    public function email(){

        $email_id    = $this->request->data("email_id");
		$email_subject  = $this->request->data("email_subject");
        $email_body  = $this->request->data("email_body");
		$feedID = $this->request->data("feed_id");

        $result=$this->feeds->email(Session::getUserUniqueID(),Session::getUserCompany(),$email_id,$email_subject,$email_body,$feedID);

        if(!$result){
            Session::set('email-errors', $this->feeds->errors());
        }
		else{
            Session::set('email-success', "Email has been created");
        }

        return $this->redirector->root("Feeds");
    }
	
	  /**
     * Email Feed
     *
     */
    public function forwardFeed(){

		$logger_group  = $this->request->data("logger_group");
		$feedID = $this->request->data("feed_id");

        $result=$this->feeds->forwardFeed(Session::getUserUniqueID(),Session::getUserCompany(),$logger_group,$feedID);

        if(!$result){
            Session::set('forward-errors', $this->feeds->errors());
        }
		else{
            Session::set('forward-success', "Feed Successfully Forwarded");
        }

        return $this->redirector->root("Feeds");
    }
	

    /**
     * update a post
     *
     */
    public function update(){

        $postId  = $this->request->data("post_id");
        $title   = $this->request->data("title");
        $content = $this->request->data("content");

        $postId = Encryption::decryptId($postId);

        if(!$this->post->exists($postId)){
            return $this->error(404);
        }

        $post = $this->post->update($postId, $title, $content);

        if(!$post){

            Session::set('posts-errors', $this->post->errors());
            return $this->redirector->root("Posts/View/" . urlencode(Encryption::encryptId($postId)) . "?action=update");

        }else{
            return $this->redirector->root("Posts/View/" . urlencode(Encryption::encryptId($postId)));
        }
    }
	
public function archive(){
       $feedId = $this->request->data("feed_id");
       $feedsArchive = $this->feeds->archiveById($feedId);

        if(!$feedsArchive){
            $this->view->renderErrors($this->feeds->errors());
        } else{
            $feedsAll= $this->feeds->getAll();
            $feedsHTML = $this->view->render(Config::get('VIEWS_PATH') . 'feeds/feeds.php', array("feeds" => $feedsAll['feeds']));
            $this->view->renderJson(array("data" => ["feedsHTML" => $feedsHTML]));
        }
    }


  

    public function isAuthorized(){

        $action = $this->request->param('action');
        $role = Session::getUserRole();
        $resource = "posts";

        // only for admins
        Permission::allow('admin', $resource, ['*']);

        // only for normal users
        Permission::allow('user', $resource, ['index', 'view', 'newPost', 'create']);
        Permission::allow('user', $resource, ['update', 'delete'], 'owner');

        $postId  = ($action === "delete")? $this->request->param("args")[0]: $this->request->data("post_id");
        if(!empty($postId)){
            $postId = Encryption::decryptId($postId);
        } 

        $config = [
            "user_id" => Session::getUserId(),
            "table" => "posts",
            "id" => $postId
        ];

        return Permission::check($role, $resource, $action, $config);
    }
}

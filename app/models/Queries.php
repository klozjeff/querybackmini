<?php

 /**
  * Queries Class/Model
  *
  * 
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Queries extends Model{

    public $table = "qb_chats";
	public $files_table = "qb_chat_messages";
     /**
      * get all Chats
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the chats, and Pagination Object.
      *
      */
     public function getAll($pageNum = 1){

         $pagination = Pagination::pagination("qb_chats", "WHERE receiver_id='".Session::getUserCompany()."'", [], $pageNum);
         $offset     = $pagination->getOffset();
         $limit      = $pagination->perPage;
        $database   = Database::openConnection();
         $query  = "SELECT qb_chats.*";
         $query .= "FROM qb_chats";
         $query .= " WHERE receiver_id='".Session::getUserCompany()."' ";
         $query .= "ORDER BY qb_chats.last_activity_time DESC ";
         $query .= "LIMIT $limit OFFSET $offset";
         $database->prepare($query);
         $database->execute();
         $chats = $database->fetchAllAssociative();
         return array("chats" => $chats, "pagination" => $pagination);
     }

     /**
      * append number of messages to the array of queries for each query.
      *
      * @access private
      * @param  array
      *
      */
     private function appendNumberOfMessages(&$chats){

         $chatId = 0;
         $database = Database::openConnection();

         $query  = "SELECT COUNT(*) AS qb_chat_messages FROM qb_chat_messages WHERE chats_id = :chat_id ";
         $database->prepare($query);
         $database->bindParam(':chat_id', $chatId);

         foreach($chats as $key => $chat){
             $chatId = (int)$chats[$key]["id"];
             $database->execute();
             $chats[$key]["qb_chat_messages"] = $database->fetchAssociative()["qb_chat_messages"];
         }
     }

     /**
      * get chat by Id.
      *
      * @access public
      * @param  integer  $chatId
      * @return array    Array holds the data of the feed
      */
	  
     public function getChatMessages($senderId,$chatId){
         $database = Database::openConnection();
		 $query  = "SELECT qb_chat_messages.*,qb_chats.chatCategory,qb_chats.unique_chat_id";
         $query .= " FROM qb_chat_messages ";
		 $query .= "INNER JOIN qb_chats ON qb_chat_messages.chat_id=qb_chats.unique_chat_id";
         $query .= " WHERE qb_chats.receiver_id='".Session::getUserCompany()."' 
AND qb_chats.sender_id='".$senderId."' 
AND (qb_chat_messages.sender_id = '".$senderId."' OR qb_chat_messages.sender_id='".Session::getUserCompany()."') 
ORDER BY id ASC";
         $database->prepare($query);
         $database->execute();
         $messages = $database->fetchAllAssociative();
         return array("messages"=>$messages);
     }
	 
	 
	    public function uploads($userId,$fileData)
	{

     // upload
	 $extn = explode(".", strtolower($fileData['name']));
	if($extn=='jpg' || $extn=='jpeg' || $extn=='png' || $extn=='gif')
	{
		
	 $fileID=sha1(uniqid(mt_rand(), true));
		 $file = Uploader::uploadPicture($fileData,$fileID);	
	}
	else
	{
		
		 $file = Uploader::uploadCSV($fileData);	
	}
       
        if(!$file) {
            $this->errors = Uploader::errors();
            return false;
        }
		
	    return true;
	}

     /**
      * Send Query
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function sendMessage($senderId,$chatId,$message,$filename,$texttype){

         $validation = new Validation();
         if(!$validation->validate([
             'Sender ID'   => [$senderId, "required"],
			 'Chat ID'   => [$chatId, "required"],
			 'Message'   => [$message, "required"],
			 'Message Type' => [$texttype, "required"]])) {
             $this->errors = $validation->errors();
             return false;
         }
		 /*if($filename!=""){
		 $fileID=sha1(uniqid(mt_rand(), true));
		 $file = Uploader::uploadPicture($filename,$fileID);
        if(!$file) {
            $this->errors = Uploader::errors();
            return false;
        }
		 }*/
         $database = Database::openConnection();
		 $messageId=sha1(uniqid(mt_rand(), true));
		 $query    = "INSERT INTO qb_chat_messages(unique_message_id,chat_id,message,message_type,media_url,sender_id,time)
		 VALUES (:msg_id,:cid,:msg,:mtype,:murl,:sender,:leo)";
         $database->prepare($query);
         $database->bindValue(':msg_id', $messageId);
		  $database->bindValue(':cid', $chatId);
         $database->bindValue(':msg', $message);
		  $database->bindValue(':mtype', $texttype);
         $database->bindValue(':murl', $filename);
		 $database->bindValue(':sender', $senderId);
		  $database->bindValue(':leo', time());
         $database->execute();
         if($database->countRows() !== 1 && $file){
		     Uploader::deleteFile(APP . "uploads/" .Session::getUserCompany()."/". $file["basename"]);
             throw new Exception ("Couldn't Send New Message");
         }
		
		 
         return true;
     }
	 
	   /**
      * Email Feed
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function email($userId,$companyID,$email_id,$email_subject,$email_body,$chatId){

         $validation = new Validation();
         if(!$validation->validate([
             'Email Subject'   => [$email_subject, "required|minLen(2)|maxLen(250)"],
			 'Email Body'   => [$email_body, "required|minLen(2)"],
			 'Feed ID'   => [$chatId, "required"],
             'Email ID'   => [$email_id, "required|email"]])) {
             $this->errors = $validation->errors();
             return false;
         }
		 
	
           $emailFeed=Email::sendEmail(Config::get('EMAIL_FEED'), $email_id, $email_subject,$email_body); 
	  
		 
         return true;
     }
	 
	  /**
      * Forward Feed to Logger Groups
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function forwardFeed($userId,$companyID,$logger_group,$chatId){

         $validation = new Validation();
         if(!$validation->validate([
             'Logger Group'   => [$logger_group, "required"],
			 'Feed ID'   => [$chatId, "required"]])) {
             $this->errors = $validation->errors();
             return false;
         }	 
	
         $this->feedGroupsOne($chatId,$logger_group);
	  
		 
         return true;
     }
	  public function feedGroups($uniquechatId,$loggers)
	  {
			foreach($loggers as $groups)
			{
	   	 $groupID=sha1(uniqid(mt_rand(), true));
			$database = Database::openConnection();
			$query    = "INSERT INTO qb_company_feed_groups(unique_feed_group,feed_id,group_id)
			VALUES (:g_id,:feed_id,:gid)";
			$database->prepare($query);
			$database->bindValue(':g_id', $groupID);
			$database->bindValue(':feed_id', $uniquechatId);
			$database->bindValue(':gid', $groups); 
			$database->execute();
			return true;   
			}
	
	  
	  }
	 
	   public function feedGroupsOne($uniquechatId,$loggers)
	  {
			
	   	 $groupID=sha1(uniqid(mt_rand(), true));
			$database = Database::openConnection();
			$query    = "INSERT INTO qb_company_feed_groups(unique_feed_group,feed_id,group_id)
			VALUES (:g_id,:feed_id,:gid)";
			$database->prepare($query);
			$database->bindValue(':g_id', $groupID);
			$database->bindValue(':feed_id', $uniquechatId);
			$database->bindValue(':gid', $loggers); 
			$database->execute();
			return true;   
			
	
	  
	  }
	 
	 
	  /**
     * Archive Query.
     *
     *
     * @param  integer $chatId
     * @throws Exception
     */
    public function archiveById($chatId){

        // current admin can't delete himself
        $validation = new Validation();
        if(!$validation->validate([ 'Query ID' => [$chatId, "required"]])) {
            $this->errors  = $validation->errors();
            return false;
        }
         $val=1; 
		 $database = Database::openConnection();
         $query = "UPDATE qb_chats SET isArchived = :val WHERE unique_id_feeds = :id LIMIT 1";

         $database->prepare($query);
         $database->bindValue(':val', $val);
         $database->bindValue(':id', $chatId);
         $result = $database->execute();
		 if(!$result){
             throw new Exception("Couldn't Archive Feed of ID: " . $chatId);
         }
		 return true;

    }
	
	
	/**
	
	Pass Messages to View
	**/
 public function parseEmoticons($domain,$text,$type,$file,$company) {
	if($type=="img")
	{
  $text = str_replace('<3', '&lt;3', $text);
  $text = strip_tags($text);
  $text = str_replace(':)', '<img src="'.$domain.'/img/emoticons/Smile.png" class="emoticon">', $text);
  $text = str_replace(';)', '<img src="'.$domain.'/img/emoticons/Wink.png" class="emoticon">', $text);
  $text = str_ireplace(':D', '<img src="'.$domain.'/img/emoticons/Laughing.png" class="emoticon">', $text);
  $text = str_replace('&lt;3', '<img src="'.$domain.'/img/emoticons/Heart.png" class="emoticon">', $text);
  $text = str_ireplace(':P', '<img src="'.$domain.'/img/emoticons/Crazy.png" class="emoticon">', $text);
  $text = str_replace(':$', '<img src="'.$domain.'/img/emoticons/Money-Mouth.png" class="emoticon">', $text);
  $text = str_replace(':*', '<img src="'.$domain.'/img/emoticons/Kiss.png" class="emoticon">', $text);
  $text = str_replace(':(', '<img src="'.$domain.'/img/emoticons/Frown.png" class="emoticon">', $text);
  $text = str_ireplace(':X', '<img src="'.$domain.'/img/emoticons/Sealed.png" class="emoticon">', $text);
  $text = str_ireplace('8|', '<img src="'.$domain.'/img/emoticons/Cool.png" class="emoticon">', $text);
  $text = str_ireplace('(y)', '<img src="'.$domain.'/img/emoticons/Thumbs-Up.png" class="emoticon">', $text);
  $text = str_ireplace('O:]', '<img src="'.$domain.'/img/emoticons/Innocent.png" class="emoticon">', $text);
  $text = str_ireplace(':O', '<img src="'.$domain.'/img/emoticons/Gasp.png" class="emoticon">', $text);
  $text = str_ireplace('3:]', '<img src="'.$domain.'/img/emoticons/Naughty.png" class="emoticon">', $text);
  $text = str_ireplace('8-)', '<img src="'.$domain.'/img/emoticons/Nerd.png" class="emoticon">', $text);
  $text = str_ireplace('V_V', '<img src="'.$domain.'/img/emoticons/HeartEyes.png" class="emoticon">', $text);
  $text=('<img src="'.$domain.'/'.$company.'/'.$file.'"  class="emoticon"><br/>' .$text);
	}
	else
	{
  $text = str_replace('<3', '&lt;3', $text);
  $text = strip_tags($text);
  $text = str_replace(':)', '<img src="'.$domain.'/img/emoticons/Smile.png" class="emoticon">', $text);
  $text = str_replace(';)', '<img src="'.$domain.'/img/emoticons/Wink.png" class="emoticon">', $text);
  $text = str_ireplace(':D', '<img src="'.$domain.'/img/emoticons/Laughing.png" class="emoticon">', $text);
  $text = str_replace('&lt;3', '<img src="'.$domain.'/img/emoticons/Heart.png" class="emoticon">', $text);
  $text = str_ireplace(':P', '<img src="'.$domain.'/img/emoticons/Crazy.png" class="emoticon">', $text);
  $text = str_replace(':$', '<img src="'.$domain.'/img/emoticons/Money-Mouth.png" class="emoticon">', $text);
  $text = str_replace(':*', '<img src="'.$domain.'/img/emoticons/Kiss.png" class="emoticon">', $text);
  $text = str_replace(':(', '<img src="'.$domain.'/img/emoticons/Frown.png" class="emoticon">', $text);
  $text = str_ireplace(':X', '<img src="'.$domain.'/img/emoticons/Sealed.png" class="emoticon">', $text);
  $text = str_ireplace('8|', '<img src="'.$domain.'/img/emoticons/Cool.png" class="emoticon">', $text);
  $text = str_ireplace('(y)', '<img src="'.$domain.'/img/emoticons/Thumbs-Up.png" class="emoticon">', $text);
  $text = str_ireplace('O:]', '<img src="'.$domain.'/img/emoticons/Innocent.png" class="emoticon">', $text);
  $text = str_ireplace(':O', '<img src="'.$domain.'/img/emoticons/Gasp.png" class="emoticon">', $text);
  $text = str_ireplace('3:]', '<img src="'.$domain.'/img/emoticons/Naughty.png" class="emoticon">', $text);
  $text = str_ireplace('8-)', '<img src="'.$domain.'/img/emoticons/Nerd.png" class="emoticon">', $text);
  $text = str_ireplace('V_V', '<img src="'.$domain.'/img/emoticons/HeartEyes.png" class="emoticon">', $text);
		
		
	}
  return $text;
  return $text;
}
 }

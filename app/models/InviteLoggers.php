<?php

 /**
  * Invite Loggers Class
  *
  * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
  * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
  */

class InviteLoggers extends Model{

    /**
     * get all files.
     *
     * @access public
     * @param  integer  $pageNum
     * @return array
     *
     */
    public function getAll(){

        // get Files on the Folder object
      $dir=APP.'/uploads/'.Session::getUserCompany().'/Logger_Invites/';
	if (is_dir($dir)) 
		{
		if ($dh = opendir($dir)) 
			{
			$files=readdir($dh);
			
	 
			}
		}
		 return array("files" => $files);

       
     }

    /**
     * get file by Id.
     *
     * @access public
     * @param  string  $fileId
     * @return array   Array holds the data of the file
     */
    public function getById($fileId){

        $database = Database::openConnection();
        $query  = "SELECT files.id AS id, files.filename, users.id AS user_id, users.name AS user_name, files.extension AS format, files.hashed_filename, files.date ";
        $query .= "FROM users, files ";
        $query .= "WHERE files.id = :id ";
        $query .= "AND users.id = files.user_id LIMIT 1 ";

        $database->prepare($query);
        $database->bindValue(':id', (int)$fileId);
        $database->execute();

        $file = $database->fetchAllAssociative();
        return $file;
     }

    /**
     * get file by hashed name.
     * files are unique by the hashed file name(= hash(original filename . extension)).
     *
     * @access public
     * @param  string  $hashedFileName
     * @return array   Array holds the data of the file
     */
    public function getByHashedName($hashedFileName){

        $database = Database::openConnection();

        $query  = "SELECT files.id AS id, files.filename, files.extension, files.hashed_filename ";
        $query .= "FROM  files ";
        $query .= "WHERE hashed_filename = :hashed_filename ";
        $query .= "LIMIT 1 ";

        $database->prepare($query);
        $database->bindValue(':hashed_filename', $hashedFileName);
        $database->execute();

        $file = $database->fetchAssociative();
        return $file;
    }

    /**
     * create file.
     *
     * @access public
     * @param  integer   $userId
     * @param  array     $fileData
     * @return array     Array holds the created file
     * @throws Exception If file couldn't be created
     */
    public function create($userId, $fileData)
	{

        // upload
        $file = Uploader::uploadInviteCSV($fileData);

        if(!$file) {
            $this->errors = Uploader::errors();
            return false;
        }
		
	    return true;
	}

    /**
     * deletes file.
     * This method overrides the deleteById() method in Model class.
     *
     * @access public
     * @param  array    $id
     * @throws Exception If failed to delete the file
     *
     */
    public function deleteById($id){

        $database = Database::openConnection();

        $database->getById("files", $id);
        $file = $database->fetchAssociative();

        // start a transaction to guarantee the file will be deleted from both; database and filesystem
        $database->beginTransaction();
        $database->deleteById("files", $id);

        if($database->countRows() !== 1){
            $database->rollBack();
            throw new Exception ("Couldn't delete file");
        }

        $basename = $file["hashed_filename"] . "." . $file["extension"];
        Uploader::deleteFile(APP ."uploads/" . $basename);

        $database->commit();
     }

 }
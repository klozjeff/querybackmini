<?php

 /**
  * Feeds Class/Model
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Feeds extends Model{

    public $table = "qb_company_feeds";
	public $files_table = "qb_company_feed_files";
     /**
      * get all posts
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the posts, and Pagination Object.
      *
      */
     public function getAll($pageNum = 1){

         $pagination = Pagination::pagination("qb_company_feeds", "WHERE company_id='".Session::getUserCompany()."' AND posted_by<>'' AND isArchived=0", [], $pageNum);
         $offset     = $pagination->getOffset();
         $limit      = $pagination->perPage;
        $database   = Database::openConnection();
         $query  = "SELECT qb_company_feeds.*";
         $query .= "FROM qb_company_feeds";
         $query .= " WHERE company_id='".Session::getUserCompany()."' AND posted_by<>'' AND isArchived=0 ";
         $query .= "ORDER BY qb_company_feeds.date_created DESC ";
         $query .= "LIMIT $limit OFFSET $offset";
         $database->prepare($query);
         $database->execute();
         $feeds = $database->fetchAllAssociative();
         return array("feeds" => $feeds, "pagination" => $pagination);
     }

     /**
      * append number of comments to the array of posts for each post.
      *
      * @access private
      * @param  array
      *
      */
     private function appendNumberOfComments(&$posts){

         $postId = 0;
         $database = Database::openConnection();

         $query  = "SELECT COUNT(*) AS comments FROM comments WHERE post_id = :post_id ";
         $database->prepare($query);
         $database->bindParam(':post_id', $postId);

         foreach($posts as $key => $post){
             $postId = (int)$posts[$key]["id"];
             $database->execute();
             $posts[$key]["comments"] = $database->fetchAssociative()["comments"];
         }
     }

     /**
      * get Feed by Id.
      *
      * @access public
      * @param  integer  $feedId
      * @return array    Array holds the data of the feed
      */
     public function getById($feedId){

         $database = Database::openConnection();
		  $query  = "SELECT qb_company_feeds.*";
         $query .= "FROM qb_company_feeds";
         $query .= " WHERE company_id='".Session::getUserCompany()."' AND posted_by<>'' AND isArchived=0 AND unique_id_feeds=:id ";
         $query .= "LIMIT 1 ";
         $database->prepare($query);
         $database->bindValue(':id', $feedId);
         $database->execute();
         $feed = $database->fetchAssociative();
         return $feed;
     }

     /**
      * create Feed
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function create($userId,$companyID,$title,$loggers,$content,$fileData){

         $validation = new Validation();
         if(!$validation->validate([
             'Title'   => [$title, "required|minLen(2)|maxLen(250)"],
             'Content'   => [$content, "required|minLen(4)"]])) {
             $this->errors = $validation->errors();
             return false;
         }
		 
		 $fileID=sha1(uniqid(mt_rand(), true));
		 $image = Uploader::uploadPicture($fileData,$fileID);
        if(!$image) {
            $this->errors = Uploader::errors();
            return false;
        }

         $database = Database::openConnection();
		 $uniqueFeedID=sha1(uniqid(mt_rand(), true));
         $query    = "INSERT INTO qb_company_feeds(unique_id_feeds,company_id,feed_name,posted_by,content,time)
		 VALUES (:feed_id,:cid,:title,:uid,:content,:leo)";
         $database->prepare($query);
         $database->bindValue(':feed_id', $uniqueFeedID);
		  $database->bindValue(':cid', $companyID);
         $database->bindValue(':title', $title);
		  $database->bindValue(':uid', $userId);
         $database->bindValue(':content', $content);
		  $database->bindValue(':leo', time());
         $database->execute();
         if($database->countRows() !== 1){
		     Uploader::deleteFile(APP . "uploads/" .Session::getUserCompany()."/". $image["basename"]);
             throw new Exception ("Couldn't add New feed");
         }
		 else	 
		 {
		 $fileTableData=array('unique_id_file'=>$fileID,'feed_id'=>$uniqueFeedID,'filename'=>PUBLIC_ROOT ."uploads/" .Session::getUserCompany()."/".$image["basename"]);
		 Uploader::uploadFileToDatabase('qb_company_feed_files',$fileTableData);
		 $this->feedGroups($uniqueFeedID, $loggers);
		 }
		 
         return true;
     }
	 
	   /**
      * Email Feed
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function email($userId,$companyID,$email_id,$email_subject,$email_body,$feedID){

         $validation = new Validation();
         if(!$validation->validate([
             'Email Subject'   => [$email_subject, "required|minLen(2)|maxLen(250)"],
			 'Email Body'   => [$email_body, "required|minLen(2)"],
			 'Feed ID'   => [$feedID, "required"],
             'Email ID'   => [$email_id, "required|email"]])) {
             $this->errors = $validation->errors();
             return false;
         }
		 
	
           $emailFeed=Email::sendEmail(Config::get('EMAIL_FEED'), $email_id, $email_subject,$email_body); 
	  
		 
         return true;
     }
	 
	  /**
      * Forward Feed to Logger Groups
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $title
      * @param  string    $content
      * @return bool
      * @throws Exception If feed couldn't be created
      *
      */
     public function forwardFeed($userId,$companyID,$logger_group,$feedID){

         $validation = new Validation();
         if(!$validation->validate([
             'Logger Group'   => [$logger_group, "required"],
			 'Feed ID'   => [$feedID, "required"]])) {
             $this->errors = $validation->errors();
             return false;
         }	 
	
         $this->feedGroupsOne($feedID,$logger_group);
	  
		 
         return true;
     }
	  public function feedGroups($uniqueFeedID,$loggers)
	  {
			foreach($loggers as $groups)
			{
	   	 $groupID=sha1(uniqid(mt_rand(), true));
			$database = Database::openConnection();
			$query    = "INSERT INTO qb_company_feed_groups(unique_feed_group,feed_id,group_id)
			VALUES (:g_id,:feed_id,:gid)";
			$database->prepare($query);
			$database->bindValue(':g_id', $groupID);
			$database->bindValue(':feed_id', $uniqueFeedID);
			$database->bindValue(':gid', $groups); 
			$database->execute();
			return true;   
			}
	
	  
	  }
	 
	   public function feedGroupsOne($uniqueFeedID,$loggers)
	  {
			
	   	 $groupID=sha1(uniqid(mt_rand(), true));
			$database = Database::openConnection();
			$query    = "INSERT INTO qb_company_feed_groups(unique_feed_group,feed_id,group_id)
			VALUES (:g_id,:feed_id,:gid)";
			$database->prepare($query);
			$database->bindValue(':g_id', $groupID);
			$database->bindValue(':feed_id', $uniqueFeedID);
			$database->bindValue(':gid', $loggers); 
			$database->execute();
			return true;   
			
	
	  
	  }
	 
	 

     /**
      * update Feed
      *
      * @access public
      * @static static method
      * @param  string    $postId
      * @param  string    $title
      * @param  string    $content
      * @return array     Array of the updated Feed
      * @throws Exception If Feed couldn't be updated
      *
      */
     public function update($postId, $title, $content){

         $validation = new Validation();
         if(!$validation->validate([
             'Title'   => [$title, "required|minLen(2)|maxLen(60)"],
             'Content' => [$content, "required|minLen(4)|maxLen(1800)"]])) {
             $this->errors = $validation->errors();
             return false;
         }

         $database = Database::openConnection();
         $query = "UPDATE posts SET title = :title, content = :content WHERE id = :id LIMIT 1";

         $database->prepare($query);
         $database->bindValue(':title', $title);
         $database->bindValue(':content', $content);
         $database->bindValue(':id', $postId);
         $result = $database->execute();

         if(!$result){
             throw new Exception("Couldn't update post of ID: " . $postId);
         }

         $post = $this->getById($postId);
         return $post;
     }
	 
	  /**
     * Archive Feed.
     *
     *
     * @param  integer $feedId
     * @throws Exception
     */
    public function archiveById($feedId){

        // current admin can't delete himself
        $validation = new Validation();
        if(!$validation->validate([ 'Feed ID' => [$feedId, "required"]])) {
            $this->errors  = $validation->errors();
            return false;
        }
         $val=1; 
		 $database = Database::openConnection();
         $query = "UPDATE qb_company_feeds SET isArchived = :val WHERE unique_id_feeds = :id LIMIT 1";

         $database->prepare($query);
         $database->bindValue(':val', $val);
         $database->bindValue(':id', $feedId);
         $result = $database->execute();
		 if(!$result){
             throw new Exception("Couldn't Archive Feed of ID: " . $feedId);
         }
		 return true;

    }

 }

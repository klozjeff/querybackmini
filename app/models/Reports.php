<?php

 /**
  * Reports Class/Model
  *
  * 
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Reports extends Model{

   
     /**
      * get all QueriesbyCategory
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the QueriesbyCategory
      *
      */
     public function queriesbyCategory(){
         $database   = Database::openConnection();
         $query  = " SELECT chatCategory,COUNT(*) AS qty,COUNT(CASE WHEN isClosed='1' THEN 1 END) AS Closed,COUNT(CASE WHEN isClosed=0 THEN 1 END) AS Open ";
         $query .= "FROM qb_chats";
         $query .= " WHERE qb_chats.receiver_id!=''";
         $query .= "GROUP BY chatCategory ORDER BY chatCategory ASC";
         $database->prepare($query);
         $database->execute();
         $queries = $database->fetchAllAssociative();
         return array("queries" => $queries);
     }
	 
	 /**
      * get all Feeds Count By Group
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the Feeds Count By Group
      *
      */
     public function feedsCountByGroup(){
         $database   = Database::openConnection();
		 $query  = "SELECT qb_company_groups.group_name,(SELECT COUNT(*) FROM qb_company_feed_groups WHERE qb_company_feed_groups.group_id=qb_company_groups.id) AS feeds_qty ";
         $query .= "FROM qb_company_groups";
         $query .= " WHERE qb_company_groups.company_id!='' ";
         $query .= "GROUP BY qb_company_groups.group_name ORDER BY qb_company_groups.group_name ASC";
         $database->prepare($query);
         $database->execute();
         $feedsCount = $database->fetchAllAssociative();
         return array("feedsCount" => $feedsCount);
     }
	 
	  /**
      * get Monthly Loggers Trend 
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the Monthly Loggers Trend
      *
      */
     public function loggersTrend(){
         $database   = Database::openConnection();
		 $value="";
		  for ($m=1; $m<=12; $m++) {
     $month = date('M', mktime(0,0,0,$m, 1, date('Y')));
	 $month_numeric = date('m', mktime(0,0,0,$m, 1, date('Y')));
	 $value .= '	
<tr>
<td>'.$month.'</td>';
	$query ="SELECT COUNT(DISTINCT user_id) AS qty FROM qb_company_loggers WHERE MONTH(date_created)='".$month_numeric."'";
	$database->prepare($query);
    $database->execute();
	$result = $database->fetchAllAssociative();
foreach($result as $r) 
{
	
$value .= '<td>'.$r['qty'].'</td>
</tr>';
}
 
   
     }
	 
	  return $value;

     }
    
    

 }

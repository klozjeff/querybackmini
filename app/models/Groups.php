<?php

 /**
  * Groups Class/Model
  *
  * 
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Groups extends Model{

 
     /**
      * get all groups
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the Groups, and Pagination Object.
      *
      */
     public function getGroups($pageNum = 1,$option=null){

	   $options = empty($option) ?"":" AND ".$option;
       $pagination = Pagination::pagination("qb_company_groups", "WHERE qb_company_groups.company_id='".Session::getUserCompany()."'", [], $pageNum);
         $offset     = $pagination->getOffset();
         $limit      = $pagination->perPage;
        $database   = Database::openConnection();
         $query  = "SELECT qb_company_groups.*";
         $query .= "FROM qb_company_groups";
         $query .= " WHERE qb_company_groups.company_id='".Session::getUserCompany()."' AND isArchived=0".$options;
         $query .= " ORDER BY id DESC ";
         $query .= "LIMIT $limit OFFSET $offset";
         $database->prepare($query);
         $database->execute();
         $groups = $database->fetchAllAssociative();
         return array("groups" => $groups, "pagination" => $pagination);
     }

					

     /**
      * append number of comments to the array of groups for each group.
      *
      * @access private
      * @param  array
      *
      */
     private function appendNumberOfLoggers(&$groups){

         $groupId = 0;
         $database = Database::openConnection();

         $query  = "SELECT COUNT(*) AS comments FROM comments WHERE group_id = :group_id ";
         $database->prepare($query);
         $database->bindParam(':group_id', groupId);

         foreach($groups as $key => $group){
             $groupId = (int)$groups[$key]["id"];
             $database->execute();
             $groups[$key]["comments"] = $database->fetchAssociative()["comments"];
         }
     }

     /**
      * get Group by Id.
      *
      * @access public
      * @param  integer  groupId
      * @return array    Array holds the data of the group
      */
     public function getById($groupId){

         $database = Database::openConnection();
         $query  = "SELECT groups.id AS id, users.profile_picture, users.id AS user_id, users.name AS user_name, groups.title, groups.content, groups.date ";
         $query .= "FROM users, groups ";
         $query .= "WHERE groups.id = :id ";
         $query .= "AND users.id = groups.user_id LIMIT 1 ";

         $database->prepare($query);
         $database->bindValue(':id', $groupId);
         $database->execute();

         $group = $database->fetchAssociative();
         return $group;
     }

     /**
      * create Group
      *
      * @access public
      * @param  integer   $userId
      * @param  string    $Group Name
      * @param  string    $content
      * @return bool
      * @throws Exception If Group couldn't be created
      *
      */
     public function create($userId,$companyID,$group_name)
		{

         $validation = new Validation();
         if(!$validation->validate([
             'Group Name'   => [$group_name, "required|minLen(2)|maxLen(250)"]])) {
             $this->errors = $validation->errors();
             return false;
         }
	
         $database = Database::openConnection();
		 $uniqueGroupID=sha1(uniqid(mt_rand(), true));
         $query    = "INSERT INTO qb_company_groups(unique_id_group,company_id,group_name,created_by)
		 VALUES (:g_id,:cid,:name,:uid)";
         $database->prepare($query);
         $database->bindValue(':g_id', $uniqueGroupID);
		  $database->bindValue(':cid', $companyID);
         $database->bindValue(':name', $group_name);
		  $database->bindValue(':uid', $userId);
         $database->execute(); 
         return true;
		}
	 
	  
	  public function feedGroups($uniqueFeedID,$loggers)
	  {
			foreach($loggers as $groups)
			{
	   	 $groupID=sha1(uniqid(mt_rand(), true));
			$database = Database::openConnection();
			$query    = "INSERT INTO qb_company_feed_groups(unique_feed_group,feed_id,group_id)
			VALUES (:g_id,:feed_id,:gid)";
			$database->prepare($query);
			$database->bindValue(':g_id', $groupID);
			$database->bindValue(':feed_id', $uniqueFeedID);
			$database->bindValue(':gid', $groups); 
			$database->execute();
			return true;   
			}
	
	  
	  }
	 
	 
	 
	 

     /**
      * update group
      *
      * @access public
      * @static static method
      * @param  string    groupId
      * @param  string    $title
      * @param  string    $content
      * @return array     Array of the updated group
      * @throws Exception If group couldn't be updated
      *
      */
     public function update($groupId, $title, $content){

         $validation = new Validation();
         if(!$validation->validate([
             'Title'   => [$title, "required|minLen(2)|maxLen(60)"],
             'Content' => [$content, "required|minLen(4)|maxLen(1800)"]])) {
             $this->errors = $validation->errors();
             return false;
         }

         $database = Database::openConnection();
         $query = "UPDATE groups SET title = :title, content = :content WHERE id = :id LIMIT 1";

         $database->prepare($query);
         $database->bindValue(':title', $title);
         $database->bindValue(':content', $content);
         $database->bindValue(':id', $groupId);
         $result = $database->execute();

         if(!$result){
             throw new Exception("Couldn't update group of ID: " . $groupId);
         }

         $group = $this->getById($groupId);
         return $group;
     }

 }

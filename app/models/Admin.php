<?php

 /**
  * Admin Class
  * Admin Class inherits from User.
  *
  * @license    http://opensource.org/licenses/MIT The MIT License (MIT)
  * @author     Omar El Gabry <omar.elgabry.93@gmail.com>
  */

class Admin extends User{

    /**
     * get all users in the database
     *
     * @access public
     * @param  string  $name
     * @param  string  $email
     * @param  string  $role
     * @param  integer $pageNum
     * @return array
     *
     */
    public function getUsers($name = null, $email = null, $role = null, $pageNum = 1){

        // validate user inputs
        $validation = new Validation();
        if(!$validation->validate([
            'User Name' => [$name,  'maxLen(30)'],
            'Email'     => [$email, 'email|maxLen(50)'],
            'Role'      => [$role,  'inArray(admin, user)']])){
            $this->errors  = $validation->errors();
            return false;
        }

        // in $options array, add all possible values from user, and their name parameters
        // then applyOptions() method will see if value is not empty, then add it to our query
        $options = [
            $name      => "agentName LIKE :name ",
            $email     => "emailAddress = :email ",
            // $role      => "role = :role "
        ];

        // get options query
        $options = $this->applyOptions($options, "AND ");
        $options = empty($options)? "WHERE companyID='".Session::getUserCompany()."' AND isAgent=1": "WHERE " . $options." AND companyID='".Session::getUserCompany()."' AND isAgent=1";

        $values = [];
        if (!empty($name))  $values[":name"]  = "%". $name ."%";
        if (!empty($email)) $values[":email"] = $email;
       // if (!empty($role))  $values[":role"]  = $role;

        // get pagination object so that we can add offset and limit in our query
        $pagination = Pagination::pagination("qb_company_agent", $options, $values, $pageNum);
        $offset     = $pagination->getOffset();
        $limit      = $pagination->perPage;

        $database   = Database::openConnection();
        $query   = "SELECT id,agentID,agentName,emailAddress,isAdmin,isAgent,is_email_activated FROM qb_company_agent ";
        $query  .= $options;
        $query  .= " LIMIT $limit OFFSET $offset";

        $database->prepare($query);
        $database->execute($values);
        $agents = $database->fetchAllAssociative();

        return array("agents" => $agents, "pagination" => $pagination);
     }

    /**
     *  Update info of a passed user id
     *
     * @access public
     * @param  integer $userId
     * @param  integer $adminId
     * @param  string  $name
     * @param  string  $password
     * @param  string  $role
     * @return bool
     * @throws Exception If password couldn't be updated
     *
     */
	 
	public function createUser($name,$email,$tel,$password,$confirmPassword,$roles,$companyId)
	   {
	   
	   //Validation
	  $validation = new Validation();
        if(!$validation->validate([
            'User Name' => [$name,  'required|maxLen(250)'],
            'Email'     => [$email, 'required|email|maxLen(50)'],
			'Mobile No' => [$tel, 'required|maxLen(15)'],
			'Password' => [$password, "required|equals(".$confirmPassword.")|minLen(6)|password"],
			'Password Confirmation' => [$confirmPassword, 'required'],
            'Role'      => [$roles,'inArray(1,2,3,4,5)']])){
            $this->errors  = $validation->errors();
            return false;
        }
		
		
		//Save data into Database
		$profile='default_avatar.png';
        $database = Database::openConnection();
		$hashedPassword = password_hash($password, PASSWORD_DEFAULT, array('cost' => Config::get('HASH_COST_FACTOR')));
		$status='Active';
		$user_Id=sha1(uniqid(mt_rand(), true));
        $database->beginTransaction();
        $query = "INSERT INTO qb_company_agent(agentID,agentName,emailAddress,phoneNo,password,status,isAdmin,companyID,profile_picture,email_token,email_last_verification)".
		"VALUES (:aid,:aname,:email,:tel,:pass,:state,:isAdmin,:cid,:prof,:email_token,:email_last_verification)";
        $database->prepare($query);
		$database->bindValue(':aid',$user_Id);
        $database->bindValue(':aname',$name);
		$database->bindValue(':email', $email);
		$database->bindValue(':tel', $tel);
		$database->bindValue(':pass', $hashedPassword);
		$database->bindValue(':state', $status);
		$database->bindValue(':isAdmin',$roles);
		$database->bindValue(':cid', $companyId);
		$database->bindValue(':prof', $profile);
		// email token and time of generating it
        $token = sha1(uniqid(mt_rand(), true));
        $database->bindValue(':email_token', $token);
        $database->bindValue(':email_last_verification', time());
		
        $database->execute();
		//Send Activation Email
		$id = $database->lastInsertedId();
        //Email::sendEmail(Config::get('EMAIL_EMAIL_VERIFICATION'), $email, ["name" => $name, "id" => $id], ["email_token" => $token]); 
	     
		 $database->commit();
		 return true;
	  
	   }
	 
	 
    public function updateUserInfo($userId, $adminId, $name, $password, $role){

         $user = $this->getProfileInfo($userId);

         $name = (!empty($name) && $name !== $user["name"])? $name: null;
         $role = (!empty($role) && $role !== $user["role"])? $role: null;

         // current admin can't change his role,
         // changing the role requires to logout or reset session,
         // because role is stored in the session
         if(!empty($role) && $adminId === $user["id"]){
             $this->errors[] = "You can't change your role";
             return false;
         }

        $validation = new Validation();
        if(!$validation->validate([
             "Name" => [$name, "alphaNumWithSpaces|minLen(4)|maxLen(30)"],
             "Password" => [$password, "minLen(6)|password"],
             'Role' => [$role, "inArray(admin, user)"]])){
             $this->errors = $validation->errors();
             return false;
         }

         if($password || $name || $role) {

             $options = [
                 $name     => "name = :name ",
                 $password => "hashed_password = :hashed_password ",
                 $role     => "role = :role "
             ];

             $database = Database::openConnection();
             $query   = "UPDATE users SET ";
             $query  .= $this->applyOptions($options, ", ");
             $query  .= "WHERE id = :id LIMIT 1 ";
             $database->prepare($query);

             if($name) {
                 $database->bindValue(':name', $name);
             }
             if($password) {
                 $database->bindValue(':hashed_password', password_hash($password, PASSWORD_DEFAULT, array('cost' => Config::get('HASH_COST_FACTOR'))));
             }
             if($role){
                 $database->bindValue(':role', $role);
             }

             $database->bindValue(':id', $userId);
             $result = $database->execute();

             if(!$result){
                 throw new Exception("Couldn't update profile");
             }
         }

         return true;
     }
	 
	 /**
     * Delete a user.
     *
     * @param  string  $adminId
     * @param  integer $userId
     * @throws Exception
     */
    public function getAgentDash($userId=null){
	$options = empty($userId) ?"!=''":"="."'".$userId."'";
	    $database = Database::openConnection();
	  $query  = "SELECT * FROM (";
        $query .= "SELECT 'feeds' AS target,COUNT('unique_id_feeds') AS total_feeds FROM qb_company_feeds WHERE posted_by ".$options." AND company_id='".Session::getUserCompany()."' UNION ";
        $query .= "SELECT 'hello' AS target, COUNT(*) AS total_feeds FROM qb_chats WHERE chatCategory='Suggestion' AND isClosed=1 and assignedTo ".$options." AND receiver_id='".Session::getUserCompany()."' UNION  ";
		$query .= "SELECT 'hello2' AS target, COUNT(*) AS total_feeds FROM qb_chats WHERE chatCategory='Suggestion' AND isClosed=0 and assignedTo ".$options." AND receiver_id='".Session::getUserCompany()."' UNION ";
		 $query .= "SELECT 'hello3' AS target,COUNT(*) AS total_feeds FROM qb_chats WHERE chatCategory='Complains' AND isClosed=1 and assignedTo ".$options." AND receiver_id='".Session::getUserCompany()."' UNION ";
		$query .= "SELECT 'hello4' AS target,COUNT(*) AS total_feeds FROM qb_chats WHERE chatCategory='Complains' AND isClosed=0 and assignedTo ".$options." AND receiver_id='".Session::getUserCompany()."'";
        $query .= ") AS stats";
        $database->prepare($query);
        $database->execute();
        $stats = $database->fetchAllAssociative();
        $data =  $stats;
        return $data;
    }


    /**
     * Delete a user.
     *
     * @param  string  $adminId
     * @param  integer $userId
     * @throws Exception
     */
    public function deleteUser($adminId, $userId){

        // current admin can't delete himself
        $validation = new Validation();
        if(!$validation->validate([ 'User ID' => [$userId, "notEqual(".$adminId.")"]])) {
            $this->errors  = $validation->errors();
            return false;
        }

        $database = Database::openConnection();
        $database->deleteById("users", $userId);

        if ($database->countRows() !== 1) {
            throw new Exception ("Couldn't delete user");
        }
    }
	
	
	
	
	  /**
     * get all users in the database
     *
     * @access public
     * @param  string  $name
     * @param  string  $email
     * @param  string  $role
     * @param  integer $pageNum
     * @return array
     *
     */
    public function getAgentInfoById($userId){

        // validate  inputs
        $validation = new Validation();
        if(!$validation->validate([
            'Agent ID' => [$userId,  'required']])){
            $this->errors  = $validation->errors();
            return false;
        }
        $database   = Database::openConnection();
        $query   = "SELECT id,agentID,agentName,emailAddress,isAdmin,isAgent,is_email_activated FROM qb_company_agent ";
        $query  .= "WHERE agentID=:uid";
        $query  .= " LIMIT 1";
        $database->prepare($query);
		$database->bindValue(':uid',$userId);
        $database->execute();
        $agent = $database->fetchAllAssociative();
		
		return $agent;	
		
     }

     /**
      * Counting the number of users in the database.
      *
      * @access public
      * @static static  method
      * @return integer number of users
      *
      */
     public function countUsers(){
         return $this->countAll("users");
     }

  /**
      * Counting the number of Agents for particular Company in the database.
      *
      * @access public
      * @static static  method
      * @return integer number of Agents
      *
      */
     public function countAgents(){
	   $database = Database::openConnection();
         return $database->countAllByCompany("qb_company_agent","companyID",Session::getUserCompany(),"isAgent=1");
     }
     /**
      * Get the backup file from the backup directory in file system
      *
      * @access public
      * @return array
      */
     public function getBackups() {

         $files = scandir(APP . "backups/");
         $basename = $filename = $unixTimestamp = null;

         foreach ($files as $file) {
             if ($file != "." && $file != "..") {

                 $filename_array = explode('-', pathinfo($file, PATHINFO_FILENAME));
                 if(count($filename_array) !== 2){
                     continue;
                 }

                 // backup file has name with something like this: backup-1435788336
                 list($filename, $unixTimestamp) = $filename_array;
                 $basename = $file;
                 break;
             }
         }

         $data = array("basename" => $basename, "filename" => $filename, "date" => "On " . date("F j, Y", $unixTimestamp));
         return $data;
     }

    /**
     * Update the backup file from the backup directory in file system
     * The user of the database MUST be assigned privilege of ADMINISTRATION -> LOCK TABLES.
     *
     * @access public
     * @return bool
     */
    public function updateBackup(){

         $dir = APP . "backups/";
         $files = scandir($dir);

         // delete and clean all current files in backup directory
         foreach ($files as $file) {
             if ($file != "." && $file != "..") {
                 if (is_file("$dir/$file")) {
                     Uploader::deleteFile("$dir/$file");
                 }
             }
         }

         // you can use another username and password only for this function, while the main user has limited privileges
         $windows = true;
         if($windows){
             exec('C:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump --user=' . escapeshellcmd(Config::get('DB_USER')) . ' --password=' . escapeshellcmd(Config::get('DB_PASS')) . ' ' . escapeshellcmd(Config::get('DB_NAME')) . ' > '. APP.'backups\backup-' . time() . '.sql');
         }else{
             exec('mysqldump --user=' . escapeshellcmd(Config::get('DB_USER')) . ' --password=' .escapeshellcmd(Config::get('DB_PASS')). ' '. escapeshellcmd(Config::get('DB_NAME')) .' > '. APP . 'backups/backup-' . time() . '.sql');
         }

         return true;
     }

    /**
     * Restore the backup file
     * The user of the database MUST assigned all privileges of SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, INDEX, DROP, LOCK TABLES, & TRIGGER.
     *
     * @access public
     * @return bool
     *
     */
    public function restoreBackup(){

         $basename = $this->getBackups()["basename"];

         $validation = new Validation();
         $validation->addRuleMessage("required", "Please update backups first!");

         if(!$validation->validate(["Backup" => [$basename, "required"]])) {
             $this->errors = $validation->errors();
             return false;
         }

         $windows = true;
         if($windows){
             exec('C:\wamp\bin\mysql\mysql5.6.17\bin\mysql --user=' . escapeshellcmd(Config::get('DB_USER')) . ' --password=' . escapeshellcmd(Config::get('DB_PASS')) . ' ' . escapeshellcmd(Config::get('DB_NAME')) . ' < '.APP.'\backups\\' . $basename);
         }else{
             exec('mysql --user='.escapeshellcmd(Config::get('DB_USER')).' --password='.escapeshellcmd(Config::get('DB_PASS')).' '.escapeshellcmd(Config::get('DB_NAME')).' < '. APP . 'backups/' . $basename);
         }

         return true;
     }

    /**
     * get Agents data.
     * Use this method to download users info in database as csv file.
     *
     * @access public
     * @return array
     */
    public function getAgentsData(){

        $database = Database::openConnection();

        $database->prepare("SELECT agentID,agentName,emailAddress,isAdmin,isAgent,is_email_activated FROM qb_company_agent WHERE companyID='".Session::getUserCompany()."' AND isAgent=1");
        $database->execute();

        $users = $database->fetchAllAssociative();
        $cols  = array("Agent ID", "Agent Name", "Email", "isAdmin", "isAgent", "is Email Activated?");

        return ["rows" => $users, "cols" => $cols, "filename" => "agents"];
    }

 }
   
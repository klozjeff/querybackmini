<?php

 /**
  * Dashboard Class
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

class Dashboard extends Model{
    /**
      *
      * @var string
      */
    public function dashboard(){

        $database = Database::openConnection();

        // 1. count
        $tables = ["qb_company_feeds","qb_chats","qb_company_loggers","qb_payment"];
        $stats  = [];

        foreach($tables as $table){
		//$table=="qb_chats"? "receiver_id":"company_id"
		   if($table=='qb_chats')
		   {
		   $fieldName='receiver_id';
		   }
		   else if($table=='qb_payment')
		   {
		    $fieldName='payto';
		   }
		   else
		   {
		   
		    $fieldName='company_id';
		   }
            $stats[$table] = $database->countAllByCompany($table,$fieldName,Session::getUserCompany());
        }
        // 2. latest updates
        // Using UNION to union the data fetched from different tables.
        // @see http://www.w3schools.com/sql/sql_union.asp
        // @see (mikeY) http://stackoverflow.com/questions/6849063/selecting-data-from-two-tables-and-ordering-by-date

        // Sub Query: In SELECT, The outer SELECT must have alias, like "updates" here.
        // @see http://stackoverflow.com/questions/1888779/every-derived-table-must-have-its-own-alias
     

        $data = array("stats" => $stats);
        return $data;
    }

  
  }
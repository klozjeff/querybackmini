<?php

 /**
  * Payments Class/Model
  *
  * 
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Payshop extends Model{

   
     /**
      * get all Company Payments
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the Loggers, and Pagination Object.
      *
      */
     public function getAll($pageNum = 1){
         $pagination = Pagination::pagination("qb_payment", "WHERE qb_payment.payto!=''", [], $pageNum);
         $offset     = $pagination->getOffset();
         $limit      = $pagination->perPage;
         $database   = Database::openConnection();
		 $query  = "SELECT qb_payment.user_id,qb_payment.payfrom,qb_payment.amount,qb_payment.reference,qb_payment.payment_timestamp,qb_payment.status";
         $query .= " FROM qb_payment";
         $query .= " WHERE qb_payment.payto!=''";
         $query .= "ORDER BY payment_timestamp DESC ";
         $query .= "LIMIT $limit OFFSET $offset";
         $database->prepare($query);
         $database->execute();
         $payments = $database->fetchAllAssociative();
         return array("payments" => $payments, "pagination" => $pagination);
     }
	 
	  public function getPaymentsData(){

        $database = Database::openConnection();

        $database->prepare("SELECT qb_payment.user_id,qb_payment.payfrom,qb_payment.amount,qb_payment.reference,qb_payment.payment_timestamp,qb_payment.status FROM qb_payment WHERE qb_payment.payto!='' ORDER BY payment_timestamp DESC");
        $database->execute();

        $payments = $database->fetchAllAssociative();
        $cols  = array("Payee ID","Payment Mode","Amount","Ref No.","Date","Status");

        return ["rows" => $payments, "cols" => $cols, "filename" => "Payments"];
    }


    

 }

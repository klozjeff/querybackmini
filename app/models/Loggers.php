<?php

 /**
  * Loggers Class/Model
  *
  * 
  * @author     Geoffrey Kilonzi <geoffrey.kilonzi@g-dane.com>
  */

 class Loggers extends Model{

   
     /**
      * get all Company Loggers
      *
      * @access public
      * @param  integer  $pageNum
      * @return array    Associative array of the Loggers, and Pagination Object.
      *
      */
     public function getAll($pageNum = 1){
         $pagination = Pagination::pagination("qb_company_loggers", "WHERE qb_company_loggers.company_id='".Session::getUserCompany()."'", [], $pageNum);
         $offset     = $pagination->getOffset();
         $limit      = $pagination->perPage;
         $database   = Database::openConnection();
         $query  = "SELECT qb_users.*";
         $query .= "FROM qb_company_loggers";
		 $query .= " INNER JOIN qb_users ON qb_company_loggers.user_id=qb_users.id";
         $query .= " WHERE qb_company_loggers.company_id='".Session::getUserCompany()."'";
         $query .= "ORDER BY qb_company_loggers.id ";
         $query .= "LIMIT $limit OFFSET $offset";
         $database->prepare($query);
         $database->execute();
         $loggers = $database->fetchAllAssociative();
         return array("loggers" => $loggers, "pagination" => $pagination);
     }
	 
    /**
     * create file.
     *
     * @access public
     * @param  integer   $userId
     * @param  array     $fileData
     * @return array     Array holds the created file
     * @throws Exception If file couldn't be created
     */
    public function import($userId, $fileData)
	{

        // upload
        $file = Uploader::uploadCSV($fileData);
        if(!$file) {
            $this->errors = Uploader::errors();
            return false;
        }
		
	$fileCSV = APP . "uploads/" .Session::getUserCompany()."/". $file["basename"];
    $handle = fopen($fileCSV,"r");
	fgetcsv($handle,10000,",");
      while ($data=fgetcsv($handle,10000,",")) {
		$data0=$data[0];	
		$prof='default_avatar.png';
		$unique=$this->generatePIN();
		$pass=md5($unique);
		$database = Database::openConnection();
		$database->beginTransaction();
		$query  = "INSERT INTO qb_users(unique_id_users,telephone,password,profile_picture)
		VALUES (:unique,:phone,:pass,:pic)";
	    $database->prepare($query);
		$database->bindValue(':unique',$unique);
        $database->bindValue(':phone',$data0);
		$database->bindValue(':pass', $pass);
		$database->bindValue(':pic', $prof);
		$database->execute();
		$last_id = $database->lastInsertedId();
	   
		if($last_id!='')
		{
	     $uniqueLogID=sha1(uniqid(mt_rand(), true));
		$query="INSERT INTO qb_company_loggers(unique_id_loggers,user_id,company_id)
		VALUES (:lid,:uid,:cid)";
		$database->prepare($query);
		$database->bindValue(':lid',$uniqueLogID);
		$database->bindValue(':uid',$last_id);
        $database->bindValue(':cid',Session::getUserCompany());
		$database->execute();
		$database->commit();
		}
		
		}
		
	    return true;
	}
	 
	   /**
     * get Agents data.
     * Use this method to download users info in database as csv file.
     *
     * @access public
     * @return array
     */
    public function getLoggersData(){

        $database = Database::openConnection();

        $database->prepare("SELECT qb_users.first_name,qb_users.telephone,qb_users.email_address FROM qb_company_loggers INNER JOIN qb_users ON qb_company_loggers.user_id=qb_users.id WHERE qb_company_loggers.company_id='".Session::getUserCompany()."' ORDER BY qb_company_loggers.id");
        $database->execute();

        $loggers = $database->fetchAllAssociative();
        $cols  = array("Logger Name","Telephone","Email");

        return ["rows" => $loggers, "cols" => $cols, "filename" => "Loggers"];
    }
	
	  /**
     * get all users in the database
     *
     * @access public
     * @param  string  $name
     * @param  string  $email
     * @param  string  $role
     * @param  integer $pageNum
     * @return array
     *
     */
    public function getLoggerInfoById($loggerId){

        // validate  inputs
        $validation = new Validation();
        if(!$validation->validate([
            'Sender ID' => [$loggerId,  'required']])){
            $this->errors  = $validation->errors();
            return false;
        }
        $database   = Database::openConnection();
        $query   = "SELECT id,unique_id_users,first_name,last_name,email_address,telephone FROM qb_users ";
        $query  .= "WHERE id=:uid";
        $query  .= " LIMIT 1";
        $database->prepare($query);
		$database->bindValue(':uid',$loggerId);
        $database->execute();
        $logger = $database->fetchAllAssociative();
		return $logger;	
		
     }
	   /**
     * generate User Code.
     * @access private
     * @return value
     */
	
	private function generatePIN($digits = 4){
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}

    

 }

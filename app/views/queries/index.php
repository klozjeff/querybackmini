<section>
<div class="content-wrapper">
<div class="container">
<div class="table-grid table-grid-desktop">
<div class="col">
<div class="clearfix mb">
<div class="btn-group pull-left">
</div>
</div>
<form action="" method="post">
<div class="row topLink">
<div class="col-md-12" >
<a href="?filter=true&ticket_number=&sort_by=&agent_id=&status=0" class="pull-left" > <i class="fa fa-folder-open fa-fw"></i>Open Queries</a>

<a class="pull-left" data-toggle="modal" data-target="#filterResults"> <i class="fa fa-sliders fa-fw"></i> Filter </a> 

<a class="pull-right" > <i class="fa fa-print fa-fw"></i> Print</a> 
<a class="pull-right" > <i class="fa fa-file-excel-o fa-fw"></i>Export</a> 
<a class="pull-right" > <i class="fa fa-user-plus fa-fw"></i> Assign</a> 
<a class="pull-right" > <input type="checkbox" id="zote"> Select All</a> 
<a href="" class="pull-right" > <i class="fa fa-phone fa-fw"></i>Call Logger </a> 

</div>
</div>
<div class="panel panel-default">
<div class="panel-body">
      <?php 
											$chatsData = $this->controller->queries->getAll(empty($pageNum)? 1: $pageNum);
											echo $this->render(Config::get('VIEWS_PATH') . "queries/chats.php", array("chats" => $chatsData["chats"]));
										?>

	
	</div>
</div>
</form>
  <div class="text-right">
								<ul class="pagination">
									<?php 
										echo $this->render(Config::get('VIEWS_PATH') . "pagination/default.php", 
                                            ["pagination" => $chatsData["pagination"], "link"=> "Queries"]);
									?>
								</ul>
							</div>
</div>
</div>
</div>
</div>
</section>



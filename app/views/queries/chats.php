 <table class="table table-responsive mb-mails" id="chats">
	   <thead>
	  
	   <th style="text-align:left;">C.No</th>
	        <th style="text-align:left;">Name</th>
	         <th style="text-align:left;">Query Category</th>
                    <th style="text-align:left;">Queries</th>
					  <th style="text-align:left;"> Posted </th>
					  <th style="text-align:left;"> Agent Assigned </th>
                    <th style="text-align:left;"> Actions </th>
                    <th style="text-align:center;">  </th>
                </thead>
            <tbody>
<?php if(empty($chats))
	{ ?>
		<tr class='no-data'><td colspan='8' class='text-muted text-center'>No Queries to Show!</td></tr>
	
	<?php 
	}
	
	else{
	$t=0;
	
			foreach($chats as $chat)
			{
			$t+=1;
			?>
			<tr>
				
				<td>
	                <a href="Queries/View/<?= $chat['sender_id']?>/<?= $chat['unique_chat_id']?>" style="text-decoration:none;">
							
							
							
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-preview"><?= $chat['unique_chat_id']?></div>
								</div>
								
							</div>

						</a>
             	</td>
					<td>
						<a href="Queries/View/<?= $chat['sender_id']?>/<?= $chat['unique_chat_id']?>" style="text-decoration:none;">
						
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-subject" style="color:#515253;"><?php
									$loggerData=$this->controller->loggers->getLoggerInfoById($chat['sender_id']);
									foreach($loggerData as $logs)
										echo $logger=$logs["telephone"];
										
								
									?></div>
								</div>
								
							</div>
						</a>
					</td>
					<td><?= $chat['chatCategory']?></td>
					<td>
						<a href="Queries/View/<?= $chat['sender_id']?>/<?= $chat['unique_chat_id']?>" style="text-decoration:none;">
							
						
							<div class="mb-mail-meta">
							
								<div class="mb-mail-preview"><?= $chat['last_message']?></div>
							
							
							</div>

						</a>
					</td>
					<td><?php $this->timeAgo($chat["last_activity_time"]);?></td>
					<td><?php $agentData=$this->controller->admin->getAgentInfoById($chat['assignedTo']);
					          foreach($agentData as $agents)
					                echo $agent=$agents["agentName"];
					               
					?></td>
					<td>
						<?php if($chat['isClosed']=='0')
					{
						?>
						<a class="" href="chat/<?= $chat['sender_id']?>/<?= $chat['id']?>"> <i class="fa fa-reply"></i> </a> 
					
						  <a data-toggle="modal" data-target="#requestWriter<?= $chat['id'];?>"> 
            <i class="fa fa-user"  title="Assign to Agent"></i>
            </a>
			
			
						<a class="" href=""> <i class="fa fa-close"></i> </a>
						<?php
					}
					else
					{
					echo '<button class="btn btn-success"> <i class="fa fa-check-square-o fa-fw"></i> Ticket Resolved </button>';
	
						
					}
					?>
						</td>
					
					<td style="width:40px;">
	                <div class="checkbox c-checkbox">
	                   <label>
	                      <input name="delete[]" type="checkbox" class="checkBoxClass" value="<?= $chat['id']?>">
	                      <span class="fa fa-check"></span>
	                   </label>
	                </div>
	                	<!--Modal start-->
					<div class="modal fade" style="margin-top:10%" id="requestWriter<?= $chat['id'];?>" tabindex="-6" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<form action="" method="post" role="form" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign Ticket to Agent</h4>
      </div>
      <div class="modal-body">
	  
	  	<div class="panel-body">
  <div class="form-group has-feedback">
					<label>Select Agent</label>
					 <select name="agent_id" class="form-control">
					 <option value="">Select Agent</option>
							
    </select>
				
				
				<input type="hidden" name="ticket_id" value="<?= $chat['id'];?>" >
				
				</div>
   
      </div>
	  </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" name="assign_agent" class="qbBtn">Assign Agent</button>
      </div>
    </div>
    <input type="hidden" id="giftValue" name="giftValue">
  </form>
  </div>
</div>
					<!--Modal end-->
             	</td>
				</tr>
			
			<?php
			}
	}
			?>
			
			</tbody>

			
		</table>
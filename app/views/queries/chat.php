
	<section>
<div class="content-wrapper">
<div class="container">
<div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">Chat</div>
</div>
<div data-height="350" data-scrollable="" class="list-group">
  <div style="display: none;position: absolute;">
          <input type="file" id="photoFile" accept="" />
           
        </div>
<div id="messages">
<?php
if(empty($messages))
{
echo '<p class="text-center" style="height:90px;line-height:90px">Start Conversation</b></p>'; 	
}
else
{
	 $domain=PUBLIC_ROOT .'uploads';
	foreach($messages as $message)
	{
?>
<a href="#" class="list-group-item">
<div class="media-box">
<div class="pull-left">
  <img src="<?php echo $domain;?>/common/default_avatar.png" class="media-box-object img-rounded thumb32">
</div>
<div class="media-box-body clearfix">
  <small class="pull-right"><?= $this->timeAgo($message['time'])?><br/><br/><?= $message['chatCategory']?></small>
  <strong class="media-box-heading text-primary">
    <?php
		$company = $this->controller->user->getCompanyInfo(Session::getUserCompany());
	if($message['sender_id']==Session::getUserCompany()) {echo $company["companyName"];} else {echo $message['sender_id']; } ?></strong>
      <small><?=$this->controller->queries->parseEmoticons($domain,$message['message'],$message['message_type'],$message['media_url'],Session::getUserCompany())?></small>
    <p class="mb-sm">
    </p>
    
  </div>
</div>
</a>
<?php
	}
}
?>
</div>
</div>
<div class="panel-footer clearfix has-feedback">
<div class="input-group">
<input type="hidden" id="category" value="7844343RT" required>
<input type="hidden" id="mimi" value="<?= Session::getUserCompany();?>" required>
<input type="text" name="message" id="message" placeholder="Enter to send message" style="height:40px" class="form-control input-sm" required>
<span class="input-group-btn">
<button class="btn btn-default btn-sm" id="send" title="Send Text"><i class="fa fa-send-o"></i></button>
<button  id="photo"  class="btn btn-default btn-sm"  title="Send Image">
<i class="fa fa-photo"></i></button>
<button  id="file"  class="btn btn-default btn-sm"  title="Send File">
<i class="fa fa-file"></i></button>
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
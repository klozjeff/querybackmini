<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/bootstrap.css" id="bscss">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/app.css" id="maincss">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/theme-red.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/fonts_style.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/qb_custom.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>/css/backend/qb_responsive_inner_style.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT; ?>css/backend/fselect.css">
<style type="text/css">
.navbar-toggle .icon-bar {
    background-color: #1cb1b1;
}
.navbar-toggle.active .icon-bar {
    background-color: #fff;
}
</style>
</head>
<body class="layout">
<div class="wrapper">
		<?php require_once(Config::get('VIEWS_PATH') . "layout/default/navigation.php");?>
			<?php require_once(Config::get('VIEWS_PATH') . "layout/default/common-modals.php");?>
<?php 
	
	$notifications = $this->controller->user->getNotifications(Session::getUserId());
	$newsfeed = $posts = $files = "";
	foreach($notifications as $notification){
		if($notification["count"] > 0){
			$$notification["target"] = $notification["count"];
		}
	}
	
	$info = $this->controller->user->getProfileInfo(Session::getUserId());
	$company = $this->controller->user->getCompanyInfo(Session::getUserCompany());
	$data = $this->controller->dashboard->dashboard();
									//$updates = $data["updates"];
									$stats   = $data["stats"];
?>

	 <header class="topnavbar-wrapper"> 
  <!-- START Top Navbar-->
	
    <!-- START navbar header-->
    <div class="navbar-headerLeft">
        <a href="" class="navbar-brand">
         <img src="<?= PUBLIC_ROOT;?>images/backend/logo.png" alt="Logo" class="img-responsive"> 
        </a>
    </div>
    <div class="container responsiveTitle">
    <h1 class="companyName"><span><img src="<?=PUBLIC_ROOT .'uploads/common/'.$company['logo_picture'];?>" alt="" class="img-thumbnail img-circle user-picture"></span><?= $company['companyName'];?></h1>
     </div>
    <!-- END navbar header--> 
    <!-- START Nav wrapper-->
    <div class="navbar-headerRight">
    <div class="nav-wrapper"> 
      <!-- START Left navbar-->
      
      <!-- END Left navbar--> 
      <!-- START Right Navbar-->
      <ul class="nav navbar-nav navbar-right useHeader">
        <li>
            	<div class="globalSearch">
                	<input type="search" placeholder="search queryback" />
                    <a href="#"><em class="fa fa-fw fa-search"></em></a>
                </div>
            </li>
       
        <li class="dropdown dropdown-list" onclick="readNotifications()"> <a href="#" data-toggle="dropdown"> <em class="icon-bell" style="background-color: rgb(27, 176, 176);
padding: 10px;
border-radius: 50%;"></em>
          <div class="label label-danger" id="notifications_number">0</div>
          </a> 
          
          <!-- START Dropdown menu-->
          <ul class="dropdown-menu animated flipInX">
          	
            <li> 
              <!-- START list group-->
              <div class="list-group">
                <p style="margin: 0;text-align: center;color: #21c0c0;"> No new Notifications </p>
             
              </div>
           
            </li>
          </ul>
    
        </li>

        
        <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown" aria-expanded="false"> <em class="fa fa-fw customMenuIcon fa-bars "></em> </a>
          <ul class="dropdown-menu animated flipInX">
            <li><a href="<?= PUBLIC_ROOT . "User/Profile"; ?>"><i class="fa fa-fw fa-user"></i>Edit profile</a></li>
            <li><a href=""><i class="fa fa-fw fa-cogs"></i>Setting</a></li>
            <li><a href="<?= PUBLIC_ROOT . "Login/logOut"; ?>"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
          </ul>
         
        </li>
     

      </ul>

    </div>
   </div>
   
  
</header>
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="aside">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  
  <aside class="collapse navbar-collapse aside">
<div class="container">
<div class="aside-inner">
<nav data-sidebar-anyclick-close="" class="sidebar">
<ul class="nav">
<li id="Dashboard" class=""> <a href="<?= PUBLIC_ROOT . "Dashboard"; ?>"> <em class="fa fa-dashboard"></em> <span>Dashboard</span> </a> </li>
<li class=""> <a href="<?= PUBLIC_ROOT . "Queries"; ?>"> <em class="icon-bubbles"></em> <span>Queries</span> </a> </li>
<?php if(Session::getUserisAdmin()  == 1) {?>
<li id="users" class=""> <a href="<?= PUBLIC_ROOT . "Admin/Users"; ?>"> <em class="icon-emoticon-smile"></em> <span>Agents</span> </a> </li>
<?php } ?>
<li class=""> <a href="<?= PUBLIC_ROOT . "Loggers"; ?>"> <em class="icon-magic-wand"></em> <span>Loggers</span> </a> </li>
<li class=""> <a href="<?= PUBLIC_ROOT . "Feeds"; ?>"> <em class="fa fa-book"></em> <span>Feeds</span> </a> </li>
<?php if(Session::getUserisAdmin()  == 1) {?>

<li class=""> <a href="<?= PUBLIC_ROOT . "Payshop"; ?>"> <em class="fa fa-credit-card"></em> <span>Payshop</span> </a> </li>
<li class=""> <a href="<?= PUBLIC_ROOT . "Reports"; ?>"> <em class="fa fa-clipboard"></em> <span>Reports</span> </a> </li>

<?php } ?>
<li class=""> <a href="<?= PUBLIC_ROOT . "Feeds/newFeed"; ?>"> <em class="fa fa-addFeed"></em> <span>Create Feed</span> </a> </li>
</nav>
</div>
</div>
</aside>
<div class="container dashboardContentContainer" style="margin-top:20px;">
  <div class="row dashboardWidget">
    <div class="col-md-3">
      <div class="panel widget panelelements widget">
        <div class="row row-table">
          <div class="col-xs-4 text-center pv-lg"> <em class="icon-feed fa-3x"></em> </div>
          <div class="col-xs-8 pv-lg">
            <div class="h2 mt0"><?= $stats["qb_company_feeds"]; ?></div>
            <h2>Total Feeds</h2>
            <div class="percentage text-muted">
              <h6>32% Last Week</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel widget panelelements widget">
        <div class="row row-table">
          <div class="col-xs-4 text-center pv-lg"> <em class="icon-user fa-3x"></em> </div>
          <div class="col-xs-8 pv-lg">
            <div class="h2 mt0"><?= $stats["qb_company_loggers"]; ?></div>
            <h2>Total Loggers</h2>
            <div class="percentage text-muted">
              <h6>76% Last Week</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel widget panelelements ">
        <div class="row row-table">
          <div class="col-xs-4 text-center pv-lg"> <em class="icon-bubbles fa-3x"></em> </div>
          <div class="col-xs-8 pv-lg">
            <div class="h2 mt0"><?= $stats["qb_chats"]; ?></div>
            <h2>Total Queries</h2>
            <div class="percentage text-muted">
              <h6>6% Last Week</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="panel widget panelelements ">
        <div class="row row-table">
          <div class="col-xs-4 text-center pv-lg"> <em class="icon-credit-card fa-3x"></em> </div>
          <div class="col-xs-8 pv-lg">
            <div id="refresh2">
              <div id="time2">
                <div class="h2 mt0"><?= $stats["qb_payment"]; ?></div>
              </div>
            </div>
            <h2>Total Payments</h2>
            <div class="percentage text-muted">
              <h6>23% Last Week</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   <footer>
 <div class="pull-right">
			A product of <a href="http://g-dane.com" target="_blank">G-Dane Technologies LLC</a>
		</div>
		<div class="pull-left">
			<?= date('Y');?> &copy; All rights reserved
		</div>
 
</footer>

	
	
   </div>
	<!-- /#wrapper -->
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.min.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/bootstrap.min.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery-ui.min.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/custom-file-input.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/fSelect.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/paging.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.slimscroll.min.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/app.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.storageapi.js"></script>
<script src="<?= PUBLIC_ROOT; ?>js/common/main.js"></script>	
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.highcharts.js" type="text/javascript"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.exporting.js" type="text/javascript"></script>
<script src="<?= PUBLIC_ROOT; ?>js/backend/jquery.highchartTable.js" type="text/javascript"></script>
        <!-- Assign CSRF Token to JS variable -->
		<?php Config::addJsConfig('csrfToken', Session::generateCsrfToken()); ?>
        <!-- Assign all configration variables -->
		<script>config = <?= json_encode(Config::getJsConfig()); ?>;</script>
        <!-- Run the application -->
		<!--Script for multiple select with checkbox-->
		<script>
(function($) {
    $(function() {
        $('#test,#feeds').fSelect();
    });
})(jQuery);
</script>
  <script type="text/javascript">
$("#zote").click(function () {
    $(".checkBoxClass").prop('checked', $(this).prop('checked'));
});
</script>
<script type="text/javascript">

$(document).ready(function() {
    $('#referNav li').on('click', changeClass);
});

function changeClass() {
   $('#referNav li').removeClass('active');
    $(this).addClass('active');
}
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart').highchartTable();
  $(".navbar-toggle").click(function(){
	  $(this).toggleClass("active");
	  
	  })
});


</script>
<!--End-->
        <script>$(document).ready(app.init());</script>

		<?php Database::closeConnection(); ?>
	</body>
</html>

<form action="<?= PUBLIC_ROOT .'groups/create';?>" method="POST" id="submitPaymentForm">
  <div class="modal fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Create Sub Group</h4>
        </div>
        <div class="modal-body">
          <div class="payment-errors text-danger"></div>
          <div id="energy">
         Sub  Group Name<br>
            <input name="group_name" class="form-control">
             <br>
          </div>
        <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
        </div>
        <div class="modal-footer" id="formFooter">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="submit" class="qbBtn">Create Sub Group</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!--Import Contacts-->

<form action="<?= PUBLIC_ROOT .'loggers/import';?>" method="post" id="importContactsForm" enctype="multipart/form-data">
  <div class="modal fade" id="importContacts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Import Contacts</h4>
        </div>
         <div class="modal-body">
	  
	  	<div class="panel-body">
  <div class="form-group has-feedback">
					<label>Select File to Import</label><br/>
					 <td><a href="uploads/template/import_contacts.csv">DOWNLOAD CSV UPLOAD TEMPLATE HERE</a></td><br/><br/>
				<input type="file" name="file" id="file-2" class="inputfile inputfile-2" style="display:none"  />
                        <label for="file-2"><span>Upload CSV File</span></label>
				</div>
				
				<div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
   
      </div>
	  </div>
        <div class="modal-footer" id="formFooter">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="submit" class="qbBtn">Import Contacts</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!--End Modal Import Contacts-->
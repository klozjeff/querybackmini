<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="<?= PUBLIC_ROOT; ?>js/front/jquery.pagepiling.min.js"></script> 
<script src="<?= PUBLIC_ROOT; ?>js/front/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= PUBLIC_ROOT; ?>js/front/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?= PUBLIC_ROOT; ?>js/front/jquery.animate-enhanced.min.js"></script>
<script type="text/javascript" src="<?= PUBLIC_ROOT; ?>js/front/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= PUBLIC_ROOT; ?>js/front/custom.js" type="text/javascript"></script>
<script src="<?= PUBLIC_ROOT; ?>js/common/main.js"></script>	
        <!-- Assign CSRF Token to JS variable -->
		<?php Config::addJsConfig('csrfToken', Session::generateCsrfToken()); ?>
        <!-- Assign all configration variables -->
		<script>config = <?= json_encode(Config::getJsConfig()); ?>;</script>
        <!-- Run the application -->
        <script>$(document).ready(app.init());</script>

		<?php Database::closeConnection(); ?>
	</body>
</html>

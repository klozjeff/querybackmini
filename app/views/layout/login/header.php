<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Query Back - Login</title>
<link rel="icon" type="image/png" sizes="16x16" href="<?= PUBLIC_ROOT;?>images/front/qb-favicon-16x16.png">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/jquery.pagepiling.css" type="text/css" />
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/superslides.css">
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/qb_responsive_style.css" type="text/css" />
<link rel="stylesheet" href="<?= PUBLIC_ROOT;?>css/front/qb_style.css" type="text/css" />
</head>

<body>
<header class="top-header">
  <div class="container">
    <div class="row">
      <div class="col-xs-5 header-logo"> <a href="#"><img src="<?= PUBLIC_ROOT;?>images/front/qb_logo_big.png" alt="" class="img-responsive logo"></a> </div>
      <div class="col-md-7">
        <nav class="navbar navbar-default qbNavBar" style=" margin-bottom: 0px;">
          <div class="container-fluid nav-bar"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul id="menu" class="nav navbar-nav navbar-right">
                <li data-menuanchor="page1"><a class="menu" href="#page1">Home</a></li>
                <li data-menuanchor="page2"><a class="menu" href="#page2">Features</a></li>
                <li data-menuanchor="page3"><a class="menu" href="#page3"> Contact us</a></li>
              </ul>
            </div>
            <!-- /navbar-collapse --> 
          </div>
          <!-- / .container-fluid --> 
        </nav>
      </div>
    </div>
  </div>
</header>

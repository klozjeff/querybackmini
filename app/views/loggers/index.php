<section>
  <div class="content-wrapper">
    <div class="container">
      <div class="table-grid table-grid-desktop">
      <div class ="btn-group pull-right mgnTopBtn">
<a href="groups" class="subGroupBtn pull-left">Sub-Groups</a>

    <button type="button" class="dropdown-toggle" data-toggle = "dropdown" >Action
    	<span class = "caret"></span>
      <span class = "sr-only">Toggle Dropdown</span></button>
   <ul class="dropdown-menu" role="menu">
      <li><a data-toggle="modal" data-target="#createGroup">Create Sub Group</a></li>
      <li><a href = "#">Mark As Spam</a></li>
      <li><a href="" data-toggle="modal" data-target="#importContacts">Import Contacts</a></li>
       <li><a href="<?= PUBLIC_ROOT . "downloads/loggers"?>" >Export CSV</a></li>
   </ul>
	
</div>
        <div class="row">
          <div class="col-md-12">
		  <?php
		     if(!empty(Session::get('import-success'))){
                                echo $this->renderSuccess(Session::getAndDestroy('import-success'));
                            }
		  ?>
            <form action="" method="post">
            <div class="panel panel-default">
            <div class="panel-body">
            <table class="table table-responsive mb-mails" id="chats">
          <thead>
<th>C.No</th>
<th> Name </th>
<th> Mobile Number </th>
<th> Email</th>
<th> <input type="checkbox" id="zote"></th>
</thead>
            <tbody>
              <?php 
											$loggersData = $this->controller->loggers->getAll(empty($pageNum)? 1: $pageNum);
											echo $this->render(Config::get('VIEWS_PATH') . "loggers/loggers.php", array("loggers" => $loggersData["loggers"]));
										?>
            </tbody>
            </table>
				
          </div>
	
        </div>
        </form>
			  <div class="text-right">
								<ul class="pagination">
									<?php 
										echo $this->render(Config::get('VIEWS_PATH') . "pagination/default.php", 
                                            ["pagination" => $loggersData["pagination"], "link"=> "Loggers"]);
									?>
								</ul>
							</div>
     
      </div>
    </div>
  </div>
  </div>
  </div>
</section>



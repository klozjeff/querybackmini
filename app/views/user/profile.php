<section>
<div class="content-wrapper">
<div class="container-fluid">
<div class="col-md-12">

<div class="row">
<!-- START panel-->
<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-heading">Edit profile</div>
		<div class="panel-body">
			
			<form action="<?php echo PUBLIC_ROOT; ?>User/updateProfileInfo" method="post" role="form" enctype="multipart/form-data" id="settings">
				<div class="form-group has-feedback">
					<label for="profile_picture">Profile Picture</label>
					<input type="file" name="profile_picture" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle">
				</div>
				<div class="form-group has-feedback">
					<label for="full_name">Full Name</label>
					<input type="text" name="full_name" class="form-control" value="<?php echo $info['agentName']?>" autocomplete="off" >
					
				</div>
				<div class="form-group has-feedback">
					<label for="full_name">Email Address</label>
					<input type="text" name="email" class="form-control" value="<?php echo $info['emailAddress']?>" autocomplete="off" >
					
				</div>
				<div class="form-group has-feedback">
					<label for="full_name">Mobile Phone</label>
					<input type="text" name="phone" class="form-control" value="<?php echo $info['phoneNo']?>" autocomplete="off" >
					
				</div>
			<div class="form-group">
											<input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
										</div>
				<button type="submit" name="submit" value="submit" class="qbBtn">
											Save
											</button>
			</form>
		</div>
	</div>
</div>
<!-- END panel-->
<!-- START panel-->
<div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading">Profile Picture</div>
		<div class="panel-body">
			<img src="<?= APP_ROOT.'uploads/'.$info['profile_picture'] ?>" class="center-block img-responsive img-thumbnail" height="128" width="128">
		</div>
	</div>
</div>
<!-- END panel-->

</div>

</div>
</div>
</div>
</section>
       
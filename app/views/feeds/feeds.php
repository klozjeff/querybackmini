 <table class="table table-responsive mb-mails" id="chats">
            <thead>
            <th style="text-align:left;">F.No</th>
              <th style="text-align:left;"> Date</th>
              <th style="text-align:left;">Feeds</th>
              <th style="text-align:left;"> Actions </th>
       <th style="text-align:left;"></th>
              </thead>
            <tbody>

	<?php if(empty($feeds))
	{ ?>
		<tr class='no-data'><td colspan='3' class='text-muted text-center'>There is no Feeds!</td></tr>
	
	<?php 
	}
	
	else{
	$t=0;
	
			foreach($feeds as $feed){
			$t+=1;
			?>
				<tr>
				  <td><a href="<?= PUBLIC_ROOT . "Feeds/View/" . $feed["unique_id_feeds"]; ?>" style="text-decoration:none;">
                  <div class="mb-mail-meta">
                    <div class="pull-left">
                      <div class="mb-mail-preview"><?php echo $t?></div>
                    </div>
                  </div>
                  </a></td>
			 <td><?= $this->timestamp($feed["date_created"]); ?></td>
					
					<td>
						<a href="<?= PUBLIC_ROOT . "Feeds/View/" . $feed["unique_id_feeds"]; ?>">
							<strong><?= $this->truncate($this->encodeHTML($feed["feed_name"]),250); ?></strong>
						</a><br>
						<span class="text-muted"><?= $this->truncate($this->encodeHTML($feed["content"]),250); ?></span>
					</td>
					
					<td><a class="" href="" data-toggle="modal" data-target="#forwardFeed<?php echo $feed['unique_id_feeds']?>"> <i class="fa fa-reply"></i> </a> 
					<a href="<?= PUBLIC_ROOT . "Feeds/View/" .$feed["unique_id_feeds"]; ?>"> <i class="fa fa-pencil"></i> </a> 
					<a class="" href="" data-toggle="modal" data-target="#emailFeed<?php echo $feed['unique_id_feeds']?>"> <i class="fa fa-envelope"></i> </a> 
					<a class="archiveFeed" id="<?php echo $feed['unique_id_feeds']?>"> <i class="fa fa-download fa-fw"></i> </a> 
                  <!--Start Forward Feed Modal-->
            <form action="<?php echo PUBLIC_ROOT; ?>Feeds/forwardFeed" method="POST" id="forwardFeed">
              <div class="modal fade" id="forwardFeed<?php echo $feed['unique_id_feeds']?>" tabindex="-1" style="margin-top:10%" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Select Contact List/Group</h4>
                    </div>
                    <div class="modal-body">
                      <div class="payment-errors text-danger"></div>
                      Select Group<br>
                      <select class="form-control"  name="logger_group" placeholder="Feed name" required>
                        <option value="">Select Group</option>
               <?php 
			   $groupsData = $this->controller->groups->getGroups(empty($pageNum)? 1: $pageNum);
		       $groups=$groupsData["groups"];
			   foreach($groups as $group)
			   {
				   
				   print '<option value="'.$group['unique_id_group'].'">'.$group['group_name'].'</option>';
			   }
			   ?>
			   
                      </select>
					  
					   <div class="form-group">
                                            <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                        </div>
                    </div>
                    <input type="hidden" name="feed_id" value="<?php echo $feed['unique_id_feeds'];?>" >
                    <div class="modal-footer" id="formFooter">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" name="submit" class="qbBtn">Forward Feed</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
            <!--	End Forward Feed Modal--> 
            <!--Start Email Feed Modal-->
            <form action="<?php echo PUBLIC_ROOT; ?>Feeds/create" method="POST" id="emailFeed">
              <div class="modal fade" id="emailFeed<?php echo $feed['unique_id_feeds']?>" tabindex="-1" style="margin-top:10%" role="dialog" aria-labelledby="myModalLabel">
                <?php $feedOne=$this->controller->feeds->getById($feed['unique_id_feeds']);?>
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Email Feed</h4>
                    </div>
                    <div class="modal-body">
                      <div class="payment-errors text-danger"></div>
                      <div id="energy"> To<br>
                        <input name="email_id" placeholder="Provide email contacts separated by comma" value="" class="form-control">
                        <br>
                      </div>
                      <div id="energy"> Subject<br>
                        <input name="email_subject" placeholder="Provide email contacts separated by comma" value="<?= $feedOne['feed_name'];?>" class="form-control">
                        <br>
                      </div>
                      <div id="energy"> Email Body<br>
                        <textarea name="email_body" placeholder="Provide email contacts separated by comma" class="form-control">
		<?= $feedOne['content'];?>
			</textarea>
                        <br>
                      </div>
					  	 <div class="form-group">
                                            <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                        </div>
                      
                    </div>
                    <input type="hidden" name="feed_id" value="<?php echo $feed['unique_id_feeds'];?>" >
                    <div class="modal-footer" id="formFooter">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" name="submit" class="qbBtn">Email Feed</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            
            <!--	End Email Feed Modal-->
            </td>
			 <td style="width:40px;text-align:center;"><div class="checkbox c-checkbox">
                <label>
                  <input name="delete[]" type="checkbox" class="checkBoxClass" value="<?php echo $feed['unique_id_feeds']?>">
                  <span class="fa fa-check"></span> </label>
              </div></td>
					
				</tr>
	<?php   }
		}?>


		 </tbody>
            </table>

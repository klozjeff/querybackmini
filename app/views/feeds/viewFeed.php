
                       <?php 
					 
                            $feed = $this->controller->feeds->getById($feedId);

                       		if(empty($action)){
								echo $this->render(Config::get('VIEWS_PATH') . "feeds/feed.php", array("feed" => $feed));
                       		}else if($action === "update"){
                       			echo $this->render(Config::get('VIEWS_PATH') . 'feeds/feedUpdateForm.php', array("feed" => $feed));
                       		}
						?>
                    
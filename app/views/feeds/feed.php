	<section>
  <div class="container">
 <?php if(!empty(Session::get('feeds-success'))){echo $this->renderSuccess(Session::getAndDestroy('feeds-success'));} 
 
 if(!empty(Session::get('feeds-errors'))){
                                            echo $this->renderErrors(Session::getAndDestroy('feeds-errors'));
                                        }
 ?>
		   <form action="<?php echo PUBLIC_ROOT; ?>Feeds/create" enctype="multipart/form-data"  id="form-create-post" method="post">
          <div class="panel panel-default createFeedContainer">
          <div class="panel-heading panel-title"> </div>
          <div class="panel-body">
       
            
            <div class="row">
              <div class="col-md-6">
              	<div class="form-group">
                <label> Feed Name</label>
                 <input class="form-control" value="<?= $feed['feed_name'];?>" name="feed_name" placeholder="Feed name" value="" required>
                </input>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label> Select Loggers or Sub Group</label>
                <select class="form-control" id="feeds" multiple="multiple" name="logger_group[]" placeholder="Feed name" required>
                  <optgroup label="">
            <?php 
			   $groupsData = $this->controller->groups->getGroups(empty($pageNum)? 1: $pageNum," group_name LIKE '%DEFGRP%'");
		       $groups=$groupsData["groups"];
			   foreach($groups as $group)
			   {
				   
				   print '<option value="'.$group['unique_id_group'].'">All</option>';
			   }
			   ?>
                  </optgroup>
                 <optgroup label="Sub Groups" style="text-align:left">
                  <?php 
			   $groupsData = $this->controller->groups->getGroups(empty($pageNum)? 1: $pageNum," group_name NOT LIKE '%DEFGRP%'");
		       $groups=$groupsData["groups"];
			   foreach($groups as $group)
			   {
				   
				   print '<option value="'.$group['unique_id_group'].'">'.$group['group_name'].'</option>';
			   }
			   ?>
                  </optgroup>
                
                </select>
                </div>
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
              	<div class="form-group">
                    <label style="margin-top:10px">Create Feed</label>
                    <textarea class="form-control" name="content" placeholder="Feed content..." rows="10">
                   <?= $feed['content'];?>
                    </textarea>
					<input type="submit" name="submit" class=" pull-right qbBtn" value="Send">
                                  
				</div>
				 <div class="form-group">
                                            <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                        </div>
                <div class="feedBtn">
                	<div class="box">
                        <input type="file" name="profile_picture" id="file-2" style="display:none;" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                        <label for="file-2"><span>Attach</span></label>
                    </div>
                    <a data-toggle="modal" data-target="#createGroup" > <i class="fa fa-user-plus fa-fw"></i> Create Sub Group</a> 
                    <a data-toggle="modal" data-target="#getArchivedFeeds"> <i class="fa fa-download fa-fw"></i> Get Archieved Feed</a>
                </div>
				
              </div>
            </div>
          </div>
		   </div>
        </form>
      </div>
</section>
<section>
  <div class="content-wrapper">
    <div class="container">
      <div class="table-grid table-grid-desktop">
        <div class="btn-group pull-right mgnTopBtn">
          <button type="button" class="dropdown-toggle" data-toggle = "dropdown" >Action <span class = "caret"></span> <span class = "sr-only">Toggle Dropdown</span></button>
          <ul class="dropdown-menu" role="menu">
            <li><a data-toggle="modal" data-target="#createGroup">Create Sub Group</a></li>
            <li><a href = "#">Forward Feeds</a></li>
            <li><a href = "#">Email Feeds</a></li>
          </ul>
          <a class="subGroupBtn pull-left" >
          <input type="checkbox" id="zote">
          Select All</a> </div>
        <div class="row">
          <div class="col-md-12">
            <form action="" method="post">
            <div class="panel panel-default">
            <div class="panel-body" id="feedsID">
            
              <?php 
											$feedsData = $this->controller->feeds->getAll(empty($pageNum)? 1: $pageNum);
											echo $this->render(Config::get('VIEWS_PATH') . "feeds/feeds.php", array("feeds" => $feedsData["feeds"]));
										?>
           
				
          </div>
	
        </div>
        </form>
			  <div class="text-right">
								<ul class="pagination">
									<?php 
										echo $this->render(Config::get('VIEWS_PATH') . "pagination/default.php", 
                                            ["pagination" => $feedsData["pagination"], "link"=> "Feeds"]);
									?>
								</ul>
							</div>
     
      </div>
    </div>
  </div>
  </div>
  </div>
</section>



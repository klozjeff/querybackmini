
<section>
  <div class="content-wrapper">
    <div class="container">
      <div class="table-grid-desktop">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default createFeedContainer inviteLoggersContainer">
              <div class="panel-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home"> From File</a></li>
                <!--  <li><a data-toggle="tab" href="#menu1">From Social Media</a></li>
                  <li><a data-toggle="tab" href="#menu2">From in-house CRM</a></a></li>-->
                </ul>
                <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                    <div class="table-grid table-grid-desktop">
                      <div class="col">
                        <div class="row">
                          <div class="col-md-12"> </div>
                        </div>
                        <form action="" method="post">
                          <div class="panel panel-default">
                            <div class="panel-body" id="list-files">
                              <div id="refresh">
                                <div id="time"> </div>
                              </div>
                              <table class="table table-responsive mb-mails" id="chats">
                                <thead>
                                  <tr>
                                    <th style="text-align:left;">SI.No</th>
                                    <th style="text-align:left;">File Name</th>
                                    <th style="text-align:left;">Date</th>
                                    <th style="text-align:left;">Loggers Count </th>
                                    <th style="text-align:left;">Download</th>
                                   
                                  </tr>
                                </thead>
                                <tbody>
								<?php
								include('files.php');
								?>
                   
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
					<form id="form-upload-file" method="post" enctype="multipart/form-data">
                    <div class="feedBtn pull-left">
                      <div class="box">
                        <input type="file" name="file" id="file-2" class="inputfile inputfile-2" style="display:none"  />
                        <label for="file-2"><span>Upload CSV List</span></label>
                      </div>
					  <!--<div class="progress progress-striped active display-none">
											<div class="progress-bar progress-bar-success" style="width: 0%"></div>
										</div>-->
                    </div>
						
                    <input type="submit" name="submit" class=" pull-left qbBtn" value="Invite Loggers">
					</form>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                  
                  </div>
                  <div id="menu2" class="tab-pane fade">
                 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section>
<div class="content-wrapper">
<div class="container">
<!--?php require('admin_dash.php');?-->
<div class="col-md-12">

<div class="row">
<!-- START panel-->
<div class="col-md-4">
	<div class="panel panel-default agent-dash subGroupContent" >
	
		<div class="panel-heading">Sub Groups </div>
		<div class="panel-body" data-height="350" data-scrollable="">
		
			
	<div id="groupsall">
<!-- Initiate Groups -->

    <?php 
		$groupsData = $this->controller->groups->getGroups(empty($pageNum)? 1: $pageNum);
		echo $this->render(Config::get('VIEWS_PATH') . "groups/groups.php", array("groups" => $groupsData["groups"]));
	?>

</div>
				
		
		</div>
		
	</div>
</div>
<!-- END panel-->
<!-- START panel-->

<div class="col-md-8" >

	<div class="panel panel-default agent-dash" >
		<div class="panel-heading"> Loggers</i></div>
	
		<div class="panel-body" >
	<div id="public">
 <?php 
		$loggersData = $this->controller->loggers->getAll(empty($pageNum)? 1: $pageNum);
											echo $this->render(Config::get('VIEWS_PATH') . "groups/loggers.php", array("loggers" => $loggersData["loggers"]));
										?>
</div>
		</div>
	</div>
</div>

</div>

</div>
</div>
</div>
</section>


<section>
  <div class="content-wrapper">
    <div class="container">
	<div class="row">
<div class="col-md-12">
<div class="panel panel-default reportsContainer">
<div class="panel-body">
    <!--  <div class ="btn-group pull-right mgnTopBtn">


    <button type="button" class="dropdown-toggle" data-toggle = "dropdown" >Downloads
    	<span class = "caret"></span>
      <span class = "sr-only">Toggle Dropdown</span></button>
   <ul class="dropdown-menu" role="menu">
      <li><a href="<?= PUBLIC_ROOT . "downloads/loggers"?>" >Loggers Report</a></li>
      <li><a href="<?= PUBLIC_ROOT . "downloads/agents"?>" >Agents Report</a></li>
      <li><a href="<?= PUBLIC_ROOT . "downloads/payments"?>" >Payments Report</a></li>
      
   </ul>
	
</div>-->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-6">
              <!-- MAP & BOX PANE: -->
                <?php 
				//Get QUERIES BY CATEGORY Graph
				$qData = $this->controller->reports->queriesbyCategory();
				echo $this->render(Config::get('VIEWS_PATH') . "reports/queriesbycategory.php", array("qData" => $qData["queries"]));
										
										
										?>
            </div><!-- /.col -->
			
            <!-- Left col -->
            <div class="col-md-6">
              <!-- MAP & BOX PANE -->
              <?php 
				//Get Feeds Count By Group Graph
				$fData = $this->controller->reports->feedsCountByGroup();
				echo $this->render(Config::get('VIEWS_PATH') . "reports/feedsbygroup.php", array("fData" => $fData["feedsCount"]));
										
										
										?>
            </div><!-- /.col -->

          </div><!-- /.row -->

          <div class='row'>
		  
            <div class='col-md-6'>
              <!-- USERS LIST -->
                <?php 
				//Get Monthly Loggers Trend Graph
				
				echo $this->render(Config::get('VIEWS_PATH') . "reports/loggerstrend.php");
										
										
										?>		
            </div><!-- /.col -->
			
            <div class='col-md-6'>
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Daily Metrics</h3>
                  <div class="box-tools pull-right">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="pad">
                       <table class="table table-responsive mb-mails">
  <caption></caption>
  
   <thead>
      <tr>
          <th>Daily Metrics</th>
		   <th>Value</th>
		     
		
      </tr>
   </thead>
     <tbody>
	 <?php 

	print '	
<tr>
<td>New Loggers Yesterday</td>
<td>20</td>

</tr>
<tr>
<td>Paying Loggers</td>
<td>209</td>

</tr>
<tr>
<tr>
<td>Revenue From New Loggers</td>
<td>$20000</td>

</tr>
<td>Total Loggers</td>
<td>20</td>

</tr>

';


?>
    			
					
    		
					
					
					
					
   </tbody>
  		
</table>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

       

        </section>
  </div>
  </div>
  </div>
   </div>
  </div>
  </div>
</section>



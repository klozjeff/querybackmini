<div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Queries by Category</h3>
                  <div class="box-tools pull-right">
                   
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="pad">
                     
                        	<table class="highchart"  data-graph-container-before="1" data-graph-type="column" style="display:none;">
  <caption></caption>
  
   <thead>
      <tr>
          <th>Category</th>
		   <th>Total</th>
		       <th>Resolved</th>
	 <th>Unresolved</th>
		
      </tr>
   </thead>
     <tbody>
	 <?php 
foreach($qData as $r) 
{
	print '	
<tr>
<td>'.$r['chatCategory'].'</td>
<td>'.$r['qty'].'</td>
<td>'.$r['Open'].'</td>
<td>'.$r['Closed'].'</td>
</tr>';
}
?>
    			
					
    		
					
					
					
					
   </tbody>
  		
</table>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div>
 <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Feeds Count By Group</h3>
                  <div class="box-tools pull-right">
                   
                  </div>
                </div><!-- /.box-header -->
               <div class="box-body no-padding ">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
						<div class="pad">
						<table class="highchart"  data-graph-container-before="1" data-graph-type="pie" style="display:none;">
  <caption></caption>
  
   <thead>
      <tr>
          <th>Group Name</th>
		   <th>Total Feeds</th>
		      
      </tr>
   </thead>
     <tbody>
	 
		 
	 <?php 
foreach($fData as $r) 
{
	print '	
<tr>
<td>'.$r['group_name'].'</td>
<td>'.$r['feeds_qty'].'</td>
</tr>';
}

?>
    	
					
					
					
   </tbody>
  		
</table>
         
						</div>
                    </div><!-- /.col -->				
                  </div>	
                </div><!-- /.box-body -->
              </div><!-- /.box -->
   <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Loggers per Month Trend</h3>
                  <div class="box-tools pull-right">
                   
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding ">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
						<div class="pad">
						<table class="highchart"  data-graph-container-before="1" data-graph-type="line" style="display:none" >
  <caption></caption>
  
   <thead>
      <tr>
          <th>Month</th>
		   <th>Total</th>
		      
      </tr>
   </thead>
     <tbody>
	 
	 <?php
echo $this->controller->reports->loggersTrend();
    			
		
?>		
    		
					
					
					
					
   </tbody>
  		
</table>
         
						</div>
                    </div><!-- /.col -->				
                  </div>	
                </div><!-- /.box-body -->
              </div><!--/.box -->
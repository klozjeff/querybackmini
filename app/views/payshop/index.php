<section>
  <div class="content-wrapper">
    <div class="container">
      <div class="table-grid table-grid-desktop">
      <div class ="btn-group pull-right mgnTopBtn">
<a href="<?= PUBLIC_ROOT . "downloads/payments"?>" class="subGroupBtn pull-left">Export CSV</a>

	
</div>
        <div class="row">
          <div class="col-md-12">
            <form action="" method="post">
            <div class="panel panel-default">
            <div class="panel-body">
            <table class="table table-responsive mb-mails" id="chats">
       <thead>

	   <th style="text-align:left;">T.No</th>
                    <th style="text-align:left;">Customer No</th>
					  <th style="text-align:left;">Payment Mode </th>
 <th style="text-align:left;">Amount (USD) </th>
 <th style="text-align:left;">Description</th>
 <th style="text-align:left;">Transaction Date</th>
<th style="text-align:left;">Status</th>

                </thead>
            <tbody>
              <?php 
											$paymentsData = $this->controller->payshop->getAll(empty($pageNum)? 1: $pageNum);
											echo $this->render(Config::get('VIEWS_PATH') . "payshop/payments.php", array("payments" => $paymentsData["payments"]));
										?>
            </tbody>
            </table>
				
          </div>
	
        </div>
        </form>
			  <div class="text-right">
								<ul class="pagination">
									<?php 
										echo $this->render(Config::get('VIEWS_PATH') . "pagination/default.php", 
                                            ["pagination" => $paymentsData["pagination"], "link"=> "Payshop"]);
									?>
								</ul>
							</div>
     
      </div>
    </div>
  </div>
  </div>
  </div>
</section>



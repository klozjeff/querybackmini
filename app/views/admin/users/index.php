
<section>
<div class="content-wrapper">
<div class="container">

<div class="col-md-12">

<div class="row">

<div class="col-md-3">
	<div class="panel panel-default agent-dash"  >
		<div class="panel-heading-top"> <i class="fa fa-plus"></i> Add Agents</div>
	
		<div class="panel-body" data-height="325" data-scrollable="">
			<?php if(!empty(Session::get('agent-errors'))){
                                echo $this->renderErrors(Session::getAndDestroy('agent-errors'));
                            }
							
							 if(!empty(Session::get('agent-success'))){
                                echo $this->renderSuccess(Session::getAndDestroy('agent-success'));
                            }
							?>
		
			<form action="<?php echo PUBLIC_ROOT; ?>Admin/createUser" class="myform" method="post">
			
				<div class="form-group has-feedback">
					
					<input type="text" name="name" class="form-control" placeholder="Agent Name" value="" autocomplete="off" required>
				
				</div>
				<div class="form-group has-feedback">
					
					<input type="email" name="email" class="form-control" placeholder="Email Address" value="" autocomplete="off" required>
				</div>
				<div class="form-group has-feedback">
					
					<input type="text" name="telephone" class="form-control" placeholder="Enter Mobile No" value="" autocomplete="off" required>
				</div>
				<div class="form-group has-feedback">
					
					<input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" required>
				</div>
				<div class="form-group has-feedback">
					
					<input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" autocomplete="off">
				</div>
				
				   <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
				<input type="submit" name="submit" class="btn btn-primary pull-right" value="Save">
			</form>
		</div>
		
			<!--<div class="panel-heading-bottom_b"><i class="fa fa-pencil"></i> Edit Agents  <div class="pull-right"><i class="fa fa-trash"></i> Delete Agent</div></div>-->
	</div>
</div>
<!-- START panel-->
<div class="col-md-3">
	<div class="panel panel-default agent-dash" >
		<div class="panel-heading">AGENTS</div>
		<div class="panel-body list-group" data-height="280" data-scrollable="">
		
			<form action="" method="post" role="form" enctype="multipart/form-data" id="settings">
			  <uL class="agentsListContainer" id="referNav">
					<?php 
											$agentsData = $this->controller->admin->getUsers();
											echo $this->render(Config::get('ADMIN_VIEWS_PATH') . "users/users.php", array("agents" => $agentsData["agents"]));
										?>
				
				
			</uL>
			</form>
		</div>
		<div class="panel-heading-bottom">TOTAL AGENTS: <div class="pull-right"> <?php echo $this->controller->admin->countAgents();?></div></div>
	</div>
</div>
<!-- END panel-->
<!-- START panel-->
<div id="dashboard">
	<?php 
	
											$agentsDash = $this->controller->admin->getAgentDash();
											$updates = $agentsDash;
											echo $this->render(Config::get('ADMIN_VIEWS_PATH') . "users/dashboard.php", array("updates" => $updates));
											  
										?>
</div>


</div>

</div>
</div>
</div>
</section>


       
	   
<div id="pagepiling">
  <section class="section bgSection container qb-login-container ">
    <div class="qb-login-form-contianer">
      <div class="qb-login-form-content">
        <nav class="qb-tab-nav">
          <ul>
            <li class="active"> <a href="#" class="qb-tab-company">Company</a> </li>
            <li class=""> <a href="#" class="qb-tab-Individual">Individual</a> </li>
          </ul>
		   <?php $display_form = Session::getAndDestroy('display-form'); ?>
		    
          <form method="POST" action="dashboard/sendsms.php">
            <div class="qb-form qb-individual-form">
              <div class="form-group">
                <input type="text" class="form-control" name="phoneNo" placeholder="Enter mobile number">
              </div>
              <button name="link" type="submit" class="btn">Send</button>
              <p class="qb-play-storer">Get the app on <a  data-toggle="modal" href="">Google Play Store</a></p>
            </div>
          </form>
		  
            <form action="<?php echo PUBLIC_ROOT; ?>Login/login" id="form-login" method="post" 
                            <?php if(!empty($display_form)){ echo "class='display-none'"; } ?> >
            <div class="qb-form qb-company-form active">
			 <?php 
                            if(!empty(Session::get('login-errors'))){
                                echo $this->renderErrors(Session::getAndDestroy('login-errors'));
                            }
							
                            if(!empty(Session::get('register-errors'))){
                                echo $this->renderErrors(Session::getAndDestroy('register-errors'));
                            }
                        
                        ?>
              <div class="form-group">
                <input type="email" name="email"  placeholder="Enter Your Email Address"  id="email_address" class="form-control" required>
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Enter Your Password"  required>
              </div>
			    <?php if (!empty($redirect)) { ?>
                                    <div class="form-group">
                                        <input type="hidden" name="redirect" value="<?= $this->encodeHTML($redirect); ?>" />
                                    </div>
                                <?php } ?>
								  <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
              <button type="submit" name="submit" class="btn">Sign In</button>
              <p class="qb-play-storer"> <a  data-toggle="modal" href="#forgotModal">Forgot Password?</a> Click here to <a  data-toggle="modal" href="#myModal">Sign up</a></p>
            </div>
          </form>
        </nav>
        
        <!-- Sign Up Modal -->
		<?php if(empty(Session::get('register-success'))){ ?>
        <form action="<?php echo PUBLIC_ROOT; ?>Login/register" id="form-register" method="post">
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Create Queryback Account</h4>
				  <h4 class="modal-title" id="myModalLabel">
				   
						</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <input type="text" placeholder="Enter Your Contact Name" name="name" id="contactName" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <input type="email" placeholder="Enter Your Email Address" name="email"  id="emailAddress" class="form-control" required>
                  </div>
				     <div class="form-group">
                    <input type="text" placeholder="Phone Number" name="phoneNo" id="phoneNo"  class="form-control" required>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="Company Name" name="companyName" id="companyName" class="form-control" required>
                  </div>
               <div class="form-group">
                    <input type="text" placeholder="Staff Number" name="staff"  id="staff" class="form-control" required>
                  </div>
				  <div class="form-group">
                    <input type="password" class="form-control" placeholder="Enter Your Password" name="password"  class="form-control" required>
                  </div>
                  <div class="form-group">
                    <input type="password" placeholder="Confirm Admin Password" name="confirm_password" id="confirm_password"  class="form-control" required>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="www.queryback.com/Account Name" name="companyUrl" id="companyUrl"  class="form-control" required>
                  </div>
                  <div class="form-group">
                    <select  name="country" id="country"  class="form-control" required>
                      <option type="text" value="">Select Country</option>
                      <option value="Afghanistan">Afghanistan</option>
                      <option value="Albania">Albania</option>
                      <option value="Algeria">Algeria</option>
                      <option value="American Samoa">American Samoa</option>
                      <option value="Andorra">Andorra</option>
                      <option value="Angola">Angola</option>
                      <option value="Anguilla">Anguilla</option>
                      <option value="Antartica">Antarctica</option>
                      <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                      <option value="Argentina">Argentina</option>
                      <option value="Armenia">Armenia</option>
                      <option value="Aruba">Aruba</option>
                      <option value="Australia">Australia</option>
                      <option value="Austria">Austria</option>
                      <option value="Azerbaijan">Azerbaijan</option>
                      <option value="Bahamas">Bahamas</option>
                      <option value="Bahrain">Bahrain</option>
                      <option value="Bangladesh">Bangladesh</option>
                      <option value="Barbados">Barbados</option>
                      <option value="Belarus">Belarus</option>
                      <option value="Belgium">Belgium</option>
                      <option value="Belize">Belize</option>
                      <option value="Benin">Benin</option>
                      <option value="Bermuda">Bermuda</option>
                      <option value="Bhutan">Bhutan</option>
                      <option value="Bolivia">Bolivia</option>
                      <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                      <option value="Botswana">Botswana</option>
                      <option value="Bouvet Island">Bouvet Island</option>
                      <option value="Brazil">Brazil</option>
                      <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                      <option value="Brunei Darussalam">Brunei Darussalam</option>
                      <option value="Bulgaria">Bulgaria</option>
                      <option value="Burkina Faso">Burkina Faso</option>
                      <option value="Burundi">Burundi</option>
                      <option value="Cambodia">Cambodia</option>
                      <option value="Cameroon">Cameroon</option>
                      <option value="Canada">Canada</option>
                      <option value="Cape Verde">Cape Verde</option>
                      <option value="Cayman Islands">Cayman Islands</option>
                      <option value="Central African Republic">Central African Republic</option>
                      <option value="Chad">Chad</option>
                      <option value="Chile">Chile</option>
                      <option value="China">China</option>
                      <option value="Christmas Island">Christmas Island</option>
                      <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                      <option value="Colombia">Colombia</option>
                      <option value="Comoros">Comoros</option>
                      <option value="Congo">Congo</option>
                      <option value="Congo">Congo, the Democratic Republic of the</option>
                      <option value="Cook Islands">Cook Islands</option>
                      <option value="Costa Rica">Costa Rica</option>
                      <option value="Cota DIvoire">Cote dIvoire</option>
                      <option value="Croatia">Croatia (Hrvatska)</option>
                      <option value="Cuba">Cuba</option>
                      <option value="Cyprus">Cyprus</option>
                      <option value="Czech Republic">Czech Republic</option>
                      <option value="Denmark">Denmark</option>
                      <option value="Djibouti">Djibouti</option>
                      <option value="Dominica">Dominica</option>
                      <option value="Dominican Republic">Dominican Republic</option>
                      <option value="East Timor">East Timor</option>
                      <option value="Ecuador">Ecuador</option>
                      <option value="Egypt">Egypt</option>
                      <option value="El Salvador">El Salvador</option>
                      <option value="Equatorial Guinea">Equatorial Guinea</option>
                      <option value="Eritrea">Eritrea</option>
                      <option value="Estonia">Estonia</option>
                      <option value="Ethiopia">Ethiopia</option>
                      <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                      <option value="Faroe Islands">Faroe Islands</option>
                      <option value="Fiji">Fiji</option>
                      <option value="Finland">Finland</option>
                      <option value="France">France</option>
                      <option value="France Metropolitan">France, Metropolitan</option>
                      <option value="French Guiana">French Guiana</option>
                      <option value="French Polynesia">French Polynesia</option>
                      <option value="French Southern Territories">French Southern Territories</option>
                      <option value="Gabon">Gabon</option>
                      <option value="Gambia">Gambia</option>
                      <option value="Georgia">Georgia</option>
                      <option value="Germany">Germany</option>
                      <option value="Ghana">Ghana</option>
                      <option value="Gibraltar">Gibraltar</option>
                      <option value="Greece">Greece</option>
                      <option value="Greenland">Greenland</option>
                      <option value="Grenada">Grenada</option>
                      <option value="Guadeloupe">Guadeloupe</option>
                      <option value="Guam">Guam</option>
                      <option value="Guatemala">Guatemala</option>
                      <option value="Guinea">Guinea</option>
                      <option value="Guinea-Bissau">Guinea-Bissau</option>
                      <option value="Guyana">Guyana</option>
                      <option value="Haiti">Haiti</option>
                      <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                      <option value="Holy See">Holy See (Vatican City State)</option>
                      <option value="Honduras">Honduras</option>
                      <option value="Hong Kong">Hong Kong</option>
                      <option value="Hungary">Hungary</option>
                      <option value="Iceland">Iceland</option>
                      <option value="India">India</option>
                      <option value="Indonesia">Indonesia</option>
                      <option value="Iran">Iran (Islamic Republic of)</option>
                      <option value="Iraq">Iraq</option>
                      <option value="Ireland">Ireland</option>
                      <option value="Israel">Israel</option>
                      <option value="Italy">Italy</option>
                      <option value="Jamaica">Jamaica</option>
                      <option value="Japan">Japan</option>
                      <option value="Jordan">Jordan</option>
                      <option value="Kazakhstan">Kazakhstan</option>
                      <option value="Kenya">Kenya</option>
                      <option value="Kiribati">Kiribati</option>
                      <option value="Korea">Democratic Peoples Republic of Korea</option>
                      <option value="Kuwait">Kuwait</option>
                      <option value="Kyrgyzstan">Kyrgyzstan</option>
                      <option value="Lao">Lao Peoples Democratic Republic</option>
                      <option value="Latvia">Latvia</option>
                      <option value="Lebanon" selected>Lebanon</option>
                      <option value="Lesotho">Lesotho</option>
                      <option value="Liberia">Liberia</option>
                      <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                      <option value="Liechtenstein">Liechtenstein</option>
                      <option value="Lithuania">Lithuania</option>
                      <option value="Luxembourg">Luxembourg</option>
                      <option value="Macau">Macau</option>
                      <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                      <option value="Madagascar">Madagascar</option>
                      <option value="Malawi">Malawi</option>
                      <option value="Malaysia">Malaysia</option>
                      <option value="Maldives">Maldives</option>
                      <option value="Mali">Mali</option>
                      <option value="Malta">Malta</option>
                      <option value="Marshall Islands">Marshall Islands</option>
                      <option value="Martinique">Martinique</option>
                      <option value="Mauritania">Mauritania</option>
                      <option value="Mauritius">Mauritius</option>
                      <option value="Mayotte">Mayotte</option>
                      <option value="Mexico">Mexico</option>
                      <option value="Micronesia">Micronesia, Federated States of</option>
                      <option value="Moldova">Moldova, Republic of</option>
                      <option value="Monaco">Monaco</option>
                      <option value="Mongolia">Mongolia</option>
                      <option value="Montserrat">Montserrat</option>
                      <option value="Morocco">Morocco</option>
                      <option value="Mozambique">Mozambique</option>
                      <option value="Myanmar">Myanmar</option>
                      <option value="Namibia">Namibia</option>
                      <option value="Nauru">Nauru</option>
                      <option value="Nepal">Nepal</option>
                      <option value="Netherlands">Netherlands</option>
                      <option value="Netherlands Antilles">Netherlands Antilles</option>
                      <option value="New Caledonia">New Caledonia</option>
                      <option value="New Zealand">New Zealand</option>
                      <option value="Nicaragua">Nicaragua</option>
                      <option value="Niger">Niger</option>
                      <option value="Nigeria">Nigeria</option>
                      <option value="Niue">Niue</option>
                      <option value="Norfolk Island">Norfolk Island</option>
                      <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                      <option value="Norway">Norway</option>
                      <option value="Oman">Oman</option>
                      <option value="Pakistan">Pakistan</option>
                      <option value="Palau">Palau</option>
                      <option value="Panama">Panama</option>
                      <option value="Papua New Guinea">Papua New Guinea</option>
                      <option value="Paraguay">Paraguay</option>
                      <option value="Peru">Peru</option>
                      <option value="Philippines">Philippines</option>
                      <option value="Pitcairn">Pitcairn</option>
                      <option value="Poland">Poland</option>
                      <option value="Portugal">Portugal</option>
                      <option value="Puerto Rico">Puerto Rico</option>
                      <option value="Qatar">Qatar</option>
                      <option value="Reunion">Reunion</option>
                      <option value="Romania">Romania</option>
                      <option value="Russia">Russian Federation</option>
                      <option value="Rwanda">Rwanda</option>
                      <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                      <option value="Saint LUCIA">Saint LUCIA</option>
                      <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                      <option value="Samoa">Samoa</option>
                      <option value="San Marino">San Marino</option>
                      <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                      <option value="Saudi Arabia">Saudi Arabia</option>
                      <option value="Senegal">Senegal</option>
                      <option value="Seychelles">Seychelles</option>
                      <option value="Sierra">Sierra Leone</option>
                      <option value="Singapore">Singapore</option>
                      <option value="Slovakia">Slovakia (Slovak Republic)</option>
                      <option value="Slovenia">Slovenia</option>
                      <option value="Solomon Islands">Solomon Islands</option>
                      <option value="Somalia">Somalia</option>
                      <option value="South Africa">South Africa</option>
                      <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                      <option value="Span">Spain</option>
                      <option value="SriLanka">Sri Lanka</option>
                      <option value="St. Helena">St. Helena</option>
                      <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                      <option value="Sudan">Sudan</option>
                      <option value="Suriname">Suriname</option>
                      <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                      <option value="Swaziland">Swaziland</option>
                      <option value="Sweden">Sweden</option>
                      <option value="Switzerland">Switzerland</option>
                      <option value="Syria">Syrian Arab Republic</option>
                      <option value="Taiwan">Taiwan, Province of China</option>
                      <option value="Tajikistan">Tajikistan</option>
                      <option value="Tanzania">Tanzania, United Republic of</option>
                      <option value="Thailand">Thailand</option>
                      <option value="Togo">Togo</option>
                      <option value="Tokelau">Tokelau</option>
                      <option value="Tonga">Tonga</option>
                      <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                      <option value="Tunisia">Tunisia</option>
                      <option value="Turkey">Turkey</option>
                      <option value="Turkmenistan">Turkmenistan</option>
                      <option value="Turks and Caicos">Turks and Caicos Islands</option>
                      <option value="Tuvalu">Tuvalu</option>
                      <option value="Uganda">Uganda</option>
                      <option value="Ukraine">Ukraine</option>
                      <option value="United Arab Emirates">United Arab Emirates</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="United States">United States</option>
                      <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                      <option value="Uruguay">Uruguay</option>
                      <option value="Uzbekistan">Uzbekistan</option>
                      <option value="Vanuatu">Vanuatu</option>
                      <option value="Venezuela">Venezuela</option>
                      <option value="Vietnam">Viet Nam</option>
                      <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                      <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                      <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                      <option value="Western Sahara">Western Sahara</option>
                      <option value="Yemen">Yemen</option>
                      <option value="Yugoslavia">Yugoslavia</option>
                      <option value="Zambia">Zambia</option>
                      <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <select type="text" name="sector" id="sector" class="form-control">
                      <option>Select Sector/Industry</option>
                      <option value="Tourism">Tourism</option>
                      <option value="Agriculture">Agriculture</option>
                      <option value="Banking">Banking</option>
                      <option value="Technology">Technology</option>
                      <option value="NGO">NGO</option>
                      <option value="Micro Finance">Micro Finance</option>
                      <option value="Education">Education</option>
                    </select>
                  </div>
                  
               
				   <div class="form-group">
                                    <input type="hidden" name="csrf_token" value="<?= Session::generateCsrfToken(); ?>" />
                                </div>
                </div>
                <div class="modal-footer">
                 
                  <button type="submit" name="submit"  class="btn btn-primary" style="background: #18bdbe none repeat scroll 0 0;border:none !important">Create Account</button>
                </div>
                <p>Already have an account? <a href="" class="">Sign In</a></p>
              </div>
            </div>
          </div>
        </form>
		 <?php } else { echo $this->renderSuccess(Session::getAndDestroy('register-success')); } ?>
        <!--End Company Sign Up--> 
        
        <!-- Sign Up Modal -->
        <form method="POST" action="dashboard/recover.php">
          <div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <input type="email" placeholder="Enter Your Email Address" name="emailAddress"  id="emailAddress" class="form-control" required>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" name="reset"  class="btn btn-primary" style="background: #18bdbe none repeat scroll 0 0;border:none !important">Reset Password</button>
                </div>
                <p>Already have an account? <a href="index.html" class="">Sign In</a></p>
              </div>
            </div>
          </div>
        </form>
        <!--End Company Sign Up--> 
      </div>
    </div>
    <div class="loginFooterContainer">
      <div class="loginLeftContnet loginFooterContent">
        <p>Queryback is your one-stop-shop for all your customer service needs. Whether you want to complain about a product or service, or simply offer a suggestion for improvement, you can rest assured Queryback will get it sorted for you!</p>
      </div>
      <div class="loginRightContnet loginFooterContent"> <a href="#" class="qbLogoBig"><img src="<?= PUBLIC_ROOT;?>images/front/qb_logo_big.png" alt="" /></a>
        <p> <span>Query</span> <span>Feeds</span> <span>Payshop</span> </p>
      </div>
    </div>
  </section>
  <!--Feature Page-->
  <section class="section bgSection featuresContainer">
<div id="slides">
  <ul class="slides-container">
    <li class="featuresContainerLI">
      
        <div class="featuresContentBlock">
            <div class="featuresContentView">
            <p>More than just customer service, Get satisfaction and get it now! Download <span>Queryback ...</span></p>
            <a href="#">Read More</a>
        </div>
       
    </div>
     <div class="qbFeatureContent">
        <span>Query</span>
        <span>Payshop</span>
        <span>Feeds</span>
    </div>
    </li>
    <li class="featuresContainerLI2">
          <div class="featuresContentBlock">
            <div class="featuresContentView">
            <p>More than just customer service, Get satisfaction and get it now! Download <span>Queryback ...</span></p>
            <a href="#">Read More</a>
        </div>
    </li>
    <li class="featuresContainerLI3">
      <div class="featuresContentBlock">
            <div class="featuresContentView">
            <p>More than just customer service, Get satisfaction and get it now! Download <span>Queryback ...</span></p>
            <a href="#">Read More</a>
        </div>
    </li>
  </ul>
  <nav class="slides-navigation">
    <a href="#" class="next">Next</a>
    <a href="#" class="prev">Previous</a>
  </nav>
</div>
    
  </section>
  <!--Contact US Page-->
  <section class="section bgSection contactUsContainer">
  <div class="loginFooterContainer">
      <div class="loginLeftContnet loginFooterContent">
        <p><span>Contact US</span>Queryback, 2130 Fulton Street San Francisco, CA 94117-1080 USA ....</p>
      </div>
      <div class="loginRightContnet loginFooterContent"> <a href="#" class="qbLogoBig"><img src="images/qb_logo_big.png" alt="" /></a>
        <p> <span>Query</span> <span>Feeds</span> <span>Payshop</span> </p>
      </div>
    </div>
  
  </section>
</div>
  
![Queryback](http://g-dane.co.ke/queryback/web/dashboard/img/logo.png)

# Queryback

Queryback is your one-stop-shop for all your customer service needs. Whether you want to complain about a product or service, or simply offer a suggestion for improvement, you can rest assured Queryback will get it sorted for you!